package domain;

/**
* The copyright to the computer program(s) herein is the property of AMS Create,inc.
*  Chapter05.データベースプログラミング
*  Section11.ユーザ認証
*
*
*  練習11 ユーザ認証練習
*
* Admin
*
* @author trainee
* @version 1.0
*/
public class Admin {
	private Integer id;
	private String loginId;
	private String loginPass;
	private String loginName;
	private Integer userType;

	public String getLoginName() {
		return loginName;
	}

	/**
	 *
	 * @param loginName
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getUserType() {
		return userType;
	}

	/**
	 *
	 * @param userType
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId セットする loginId
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return loginPass
	 */
	public String getLoginPass() {
		return loginPass;
	}

	/**
	 * @param loginPass セットする loginPass
	 */
	public void setLoginPass(String loginPass) {
		this.loginPass = loginPass;
	}

}
