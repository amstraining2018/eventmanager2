package domain;

public class Attends {

	private int user_id;
	private int event_id;

	/**
	 * @return user_id
	 */
	public int getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id セットする user_id
	 */
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return event_id
	 */
	public int getEvent_id() {
		return event_id;
	}

	/**
	 * @param event_id セットする event_id
	 */
	public void setEvent_id(int event_id) {
		this.event_id = event_id;
	}

}
