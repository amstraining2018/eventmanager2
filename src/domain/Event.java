package domain;

import java.util.Date;

public class Event {

	public int getRooms_id() {
		return rooms_id;
	}

	public void setRooms_id(int rooms_id) {
		this.rooms_id = rooms_id;
	}

	//eventsテーブルのidカラム
	private int id;

	//eventsテーブルのtitleカラム
	private String title;

	//eventsテーブルのstartカラム
	private Date start;

	//eventsテーブルのendカラム
	private Date end;

	//eventsテーブルのplaceカラム
	private String place;

	//eventsテーブルのgroup_idカラム
	private int group_id;

	//groupsテーブルのgroup_nameカラム
	private String group_name;

	//eventsテーブルのidカラム
	private String detail;

	//eventsテーブルのregistered_byカラム
	private int registered_by;

	//user_typesテーブルのnameカラム
	private String registered_name;
	//eventsテーブルのrooms_idカラム
	private int rooms_id;

	//eventsの参加人数
	private int attendNum;

	//以下追加
	//eventsテーブル数

	public int getAttendNum() {
		return attendNum;
	}

	public void setAttendNum(int attendNum) {
		this.attendNum = attendNum;
	}

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title セットする title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return start
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @param start セットする start
	 */
	public void setStart(Date start) {
		this.start = start;
	}

	/**
	 * @return end
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @param end セットする end
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * @return place
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * @param place セットする place
	 */
	public void setPlace(String place) {
		this.place = place;
	}

	/**
	 * @return group_id
	 */
	public int getGroup_id() {
		return group_id;
	}

	/**
	 * @param group_id セットする group_id
	 */
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

	/**
	 * @return group_name
	 */
	public String getGroup_name() {
		return group_name;
	}

	/**
	 * @param group_name セットする group_name
	 */
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	/**
	 * @return detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail セットする detail
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * @return registered_by
	 */
	public int getRegistered_by() {
		return registered_by;
	}

	/**
	 * @param registered_by セットする registered_by
	 */
	public void setRegistered_by(int registered_by) {
		this.registered_by = registered_by;
	}

	/**
	 * @return registered_name
	 */
	public String getRegistered_name() {
		return registered_name;
	}

	/**
	 * @param registered_name セットする registered_name
	 */
	public void setRegistered_name(String registered_name) {
		this.registered_name = registered_name;
	}

	/**
	 * @return created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created セットする created
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	//eventsテーブルのcreatedカラム
	private Date created;

}
