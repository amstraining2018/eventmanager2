package domain;

public class User {
	//usersテーブルのidカラム
	private int id;

	//usersテーブルのlogin_idカラム
	private String login_id;

	//usersテーブルのlogin_passカラム
	private String login_pass;

	//usersテーブルのnameカラム
	private String name;

	//usersテーブルのtype_idカラム
	private int type_id;

	//usersテーブルのnameカラム
	private String type_name;

	//usersテーブルのgroup_idカラム
	private int group_id;

	//usersテーブルのnameカラム
	private String group_name;

	private String employee_id;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return login_id
	 */
	public String getLogin_id() {
		return login_id;
	}

	/**
	 * @param login_id セットする login_id
	 */
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	/**
	 * @return login_pass
	 */
	public String getLogin_pass() {
		return login_pass;
	}

	/**
	 * @param login_pass セットする login_pass
	 */
	public void setLogin_pass(String login_pass) {
		this.login_pass = login_pass;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return type_id
	 */
	public int getType_id() {
		return type_id;
	}

	/**
	 * @param type_id セットする type_id
	 */
	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	/**
	 * @return type_name
	 */
	public String getType_name() {
		return type_name;
	}

	/**
	 * @param type_name セットする type_name
	 */
	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	/**
	 * @return group_id
	 */
	public int getGroup_id() {
		return group_id;
	}

	/**
	 * @param group_id セットする group_id
	 */
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

	/**
	 * @return group_name
	 */
	public String getGroup_name() {
		return group_name;
	}

	/**
	 * @param group_name セットする group_name
	 */
	public void setGroup_name(String group_name) {
		this.group_name = group_name;

		/**
		 * @param employee_id ゲットする
		 */
	}
	public String getEmployee_id() {
		return employee_id;
	}
	/**
	 * @param employee_id セットする
	 */

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

}
