package domain;

/*
 * UserRegistServlet専用
 */

public class UserName {
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public String getEmployee_name() {
		return employee_name;
	}
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}
	public String getGroups_name() {
		return groups_name;
	}
	public void setGroups_name(String groups_name) {
		this.groups_name = groups_name;
	}
	private String employee_id;
	private String employee_name;
	private String groups_name;
}
