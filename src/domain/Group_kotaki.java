package domain;

public class Group_kotaki {

	private int id;

	private String group_name;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return group_name
	 */
	public String getGroup_name() {
		return group_name;
	}

	/**
	 * @param group_name セットする group_name
	 */
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

}
