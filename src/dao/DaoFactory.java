package dao;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


    /**************************************取得したデータソースでDaoのインスタンス生成**************************************************/

public class DaoFactory {
	public static UserDao createUserDao() {
		return new UserDaoImpl(getDataSource());
	}

	public static EventDao createEventDao() {
		return new EventDaoImpl(getDataSource());
	}

	public static AdminDao createAdminDao() {
		return new AdminDaoImpl(getDataSource());
	}

	public static AttendsDao createAttendsDao() {
		return new AttendsDaoImpl(getDataSource());
	}
	public static RoomDao createRoomDao() {
		return new RoomDaoImpl(getDataSource());
	}

	/**************************************group2dbのデータソース取得**************************************************/
	private static DataSource getDataSource() {
		//
		InitialContext ctx = null;
		DataSource ds = null;
		try {
			//[窓口]InitialContext型のインスタンスctxを生成
			ctx = new InitialContext();
			//lookupメソッドでデータソースを取得（オブジェクトでキャスト）
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/group2db");
		} catch (NamingException e) {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException el) {
					throw new RuntimeException(el);
				}
			}
			throw new RuntimeException(e);
		}
		return ds;
	}

}
