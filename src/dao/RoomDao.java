package dao;

import java.util.List;

import domain.Room;

public interface RoomDao {


	public List<String> getRooms_id() throws Exception;
	public List<String> getRooms_name() throws Exception;
	public List<Room> getRooms_nameAndRooms_id() throws Exception;
	public int findByroom_Id(String name) throws Exception;



}
