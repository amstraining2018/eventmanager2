package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.Attends;

public class AttendsDaoImpl implements AttendsDao {

	private DataSource ds;

	public AttendsDaoImpl(DataSource ds) {
		this.ds = ds;

	}

	/*****************************ここから前田********************************************************************/
	/*
	 * attendsテーブルからevent_idを配列として取得
	 * @see dao.AttendsDao#getAttends(domain.Attends)
	 */
	@Override
	public List<Attends> getAttends(int id) throws Exception {
		List<Attends> attendsList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			//List<User> LoginInfo = userDao.getLoginInfo(login_id);で取得したものを
			//int id = LoginInfo.get(0).getId();でidに格納し、
			//格納したidの情報からAttendsのDBからuser_idの情報
			//をぬきだす。
			String sql = "SELECT event_id FROM attends WHERE user_id=" + id;
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				//現在の位置から順方向に1行移動します。ResultSetのrsは、
				//初期状態では最初の行の前に位置付けられています。
				//mapToAttendsで取得する内容は決められている。

				attendsList.add(mapToAttends(rs));
			}
		}
		return attendsList;
	}

	/**
	*
	* ResultSetをAttendsクラスにセットします
	* @return attends
	*/
	private Attends mapToAttends(ResultSet rs) throws SQLException {
		Attends attends = new Attends();
		attends.setEvent_id((Integer) rs.getObject("event_id"));

		return attends;
	}

	@Override
	public void insertAttends(Attends attends) throws Exception {
		//ds(private DataSource.ds)を"con"に接続する。
		try (Connection con = ds.getConnection()) {
			//attendsのDBに追加登録する。
			//attendsのDBの中身が"id,user_id,event_id"の順番で登録される。
			String sql = "INSERT INTO attends VALUES (null,?,?);";

			//パラメータ付きSQL文をデータベースに送るための
			//PreparedStatement stmtを生成します。
			//stmtの？の1つ目に(1, attends.getUser_id()をINTEGER型に
			//stmtの？の2つ目に(2, attends.getEvent_id()をINTEGER型を
			//代入する。
			//stmtを更新する。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attends.getUser_id(), Types.INTEGER);
			stmt.setObject(2, attends.getEvent_id(), Types.INTEGER);
			//stmt.setString(3, item.getAddress());

			stmt.executeUpdate();
		}

	}

	@Override
	public void deleteAttends(Attends attends) throws Exception {
		try (Connection con = ds.getConnection()) {
			//*************IDのリストを取得****************
			String sql1 = "SELECT id FROM attends WHERE user_id=? and event_id=?";

			PreparedStatement stmt = con.prepareStatement(sql1);

			stmt.setInt(1, attends.getUser_id());
			stmt.setInt(2, attends.getEvent_id());
			ResultSet rs = stmt.executeQuery();

			List<Integer> idList = new ArrayList<>();
			while (rs.next()) {
				idList.add(rs.getInt("id"));

			}

			//*******************************
			for (int id : idList) {

				String sql2 = "DELETE FROM attends WHERE id=?;";

				PreparedStatement stmt2 = con.prepareStatement(sql2);
				stmt2.setObject(1, id, Types.INTEGER);

				//stmt.setString(3, item.getAddress());

				stmt2.executeUpdate();
			}
		}
	}

}
