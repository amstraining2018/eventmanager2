package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.User;
import domain.UserName;

public class UserDaoImpl implements UserDao {

	private DataSource ds;
	private static final int ROW_COUNT = 5;

	public UserDaoImpl(DataSource ds) {
		this.ds = ds;

	}
	//****************************ここから前田**********************************************//
	//****************************全員で共用************************************************//

	/*
	 * login_idからuser_id（名前）とTYPE_ID(一般か否か)を取得する 5/27時点
	 * 全員で共用、includeを利用するようなので一部削除
	 * @see dao.EventDao#getUserName(java.lang.String)
	 */
	@Override
	public List<User> getLoginInfo(String login_id) throws Exception {
		List<User> loginInfo = new ArrayList<>();

		try (Connection con = ds.getConnection()) {
			//usersのid・name・type_idを取得し、
			//ログインしたときのid(login_id)をusersのLOGIN_IDと
			//照合させて取得する。

			String sql = "SELECT users.id,employee.name as name,type_id " +
					"FROM users JOIN employee ON users.employee_id=employee.employee_id " +
					"where LOGIN_ID =? ";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, login_id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				//現在の位置から順方向に1行移動します。ResultSetのrsは、
				//初期状態では最初の行の前に位置付けられています。
				//mapToLoginInfoで取得する内容は決められている。

				loginInfo.add(mapToLoginInfo(rs));
			}
		}
		return loginInfo;
	}

	private User mapToLoginInfo(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId((Integer) rs.getObject("id"));
		user.setName(rs.getString("name"));
		user.setType_id((Integer) rs.getObject("type_id"));

		return user;
	}

	//****************************ここから菱田**********************************************//
	/*
	 * 参加している人の名前を取得するメソッドmade by 前田
	 */
	@Override
	public List<User> findByAttendsId(int id) throws Exception {
		List<User> AttendsIdList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {

			String sql = "SELECT employee.name " +
					"FROM (users JOIN attends ON users.id =attends.user_id) " +
					"JOIN employee ON employee.employee_id=users.employee_id " +
					"WHERE attends.event_id=? ";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				AttendsIdList.add(mapToUser(rs));
			}

		}

		return AttendsIdList;
	}

	private User mapToUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setName(rs.getString("name"));

		return user;
	}

	@Override
	public void update(User user) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "UPDATE members SET"
					+ " name=?,age=?,address=?,type_id=?,created=now() "
					+ " WHERE id=?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, user.getName());
			stmt.setString(2, user.getLogin_id());
			stmt.setString(3, user.getLogin_pass());
			stmt.setString(4, user.getGroup_name());

			stmt.executeUpdate();
		}
	}

	@Override
	public void update_pass(User user) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "UPDATE users SET login_pass=? WHERE id=?;";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, user.getLogin_pass());
			stmt.setObject(2, user.getId());
			stmt.executeUpdate();
		}

	}

	@Override
	public int findIdByLoginId(String login_id) throws Exception {
		int user_id = 0;

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT*from users WHERE users.login_id=?;";

			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setString(1, login_id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				user_id = rs.getInt("id");
			}

		}

		return user_id;
	}
	//***********************小滝****************************

	@Override
	public int getUserManageAllPageNumber() throws Exception {
		int allPageNumber = 1;

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT count(id) AS count FROM users";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				allPageNumber = (rs.getInt("count") - 1) / ROW_COUNT + 1;
			}
		}
		return allPageNumber;
	}

	@Override
	public List<User> findForUserManage(int PageNumber) throws Exception {
		List<User> userList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {
			//sqlでUSERSのidとnameを"id","name"という名前を
			//GROUPSのnameを"group_name"という名前に
			//USERS.group_id と GROPS.idを合わせて
			// USERS.id を降順に並び替えて
			//5件ずつ取得していく。

			String sql = "SELECT 	users.id , employee.name AS name, GROUPS.name AS group_name,users.employee_id as employee_id "
					+
					"FROM(USERS JOIN employee ON employee.employee_id=users.employee_id) " +
					"JOIN GROUPS ON employee.group_id = GROUPS.id " +
					"ORDER BY USERS.id ASC  " +
					"LIMIT " + (PageNumber - 1) * +ROW_COUNT + " , " + ROW_COUNT;

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				//現在の位置から順方向に1行移動します。ResultSetのrsは、
				//初期状態では最初の行の前に位置付けられています。
				//mapToUserForUserManageで取得する内容は決められている。

				userList.add(mapToUserForUserManage(rs));
			}
		}

		return userList;
	}

	/**
	 * ResultSetをMemberクラスにセットします
	 * @return member
	 */
	private User mapToUserForUserManage(ResultSet rs) throws SQLException {
		User member = new User();

		member.setId((Integer) rs.getObject("id"));
		member.setEmployee_id(rs.getString("employee_id"));
		member.setName(rs.getString("name"));
		member.setGroup_name(rs.getString("group_name"));

		return member;
	}

	//**********************************************************
	/*****************************************5/27徐航追加***********************************/
	@Override
	public void delete(int id) throws Exception {
		try (Connection con = ds.getConnection()) {

			String sql = "DELETE FROM users WHERE id=?";
			//usersというテーブル
			//?の部分にdeleteメソッドの引数であるidを代入し、削除するというsql文をString型のsqlに格納

			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setObject(1, id);
			//sql文の?にidをセット

			stmt.executeUpdate();
			//データの削除の場合なのでexecuteUpdate

		}

	}

	@Override
	public User findById_jyo(int id) throws Exception {
		User user = new User();

		try (Connection con = ds.getConnection()) {
			//カラムの別名定義？ 選択したテーブルを結合 井出
			String sql = "SELECT users.id AS id,employee.name AS name,users.employee_id AS employee_id,groups.name AS group_name,users.login_id AS login_id,users.login_pass AS login_pass "
					+
					"FROM (users JOIN employee ON employee.employee_id = users.employee_id) " +
					"JOIN groups ON employee.group_id=groups.id " +
					"WHERE users.id=? ";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, id);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				user = mapToUser_jyo(rs);
			}
		}
		return user;
	}

	private User mapToUser_jyo(ResultSet rs) throws SQLException {
		User user = new User();

		user.setId((Integer) rs.getObject("ID"));
		user.setName(rs.getString("NAME"));
		user.setEmployee_id(rs.getString("employee_id"));
		user.setGroup_name(rs.getString("GROUP_NAME"));
		user.setLogin_id(rs.getString("LOGIN_ID"));
		user.setLogin_pass(rs.getString("LOGIN_PASS"));

		return user;
	}

	/*****************************************5/27徐航追加ここまで***********************************/
	/*****************************************5/27高橋追加ここから***********************************/
	// 髙橋が編集したところ↓ ここから

	// ユーザー登録に使う
	@Override
	public void insert(User user) throws Exception {
		//権限はデフォルトで1として固定

		try (Connection con = ds.getConnection()) {
			String sql = "INSERT INTO users"
					+ " (login_id,login_pass,employee_id,TYPE_ID,CREATED) "
					+ "VALUES(?,?,?,1,now())";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, user.getLogin_id());
			stmt.setString(2, user.getLogin_pass());
			stmt.setString(3, user.getEmployee_id());
			stmt.executeUpdate();
		}

	}

	@Override
	public void insertUser(User user) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		try (Connection con = ds.getConnection()) {
			String sql = "INSERT INTO users"
					+ " (login_id,login_pass,type_id,created,employee_id) "
					+ "VALUES(?,?,?,now(),?)";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, user.getLogin_id());
			stmt.setString(2, user.getLogin_pass());
			stmt.setObject(3, user.getType_id(), Types.INTEGER);
			stmt.setString(4, user.getEmployee_id());
			stmt.executeUpdate();
		}

	}

	/*****************************************6/11高橋追加ここまで***********************************/
	/*****************************************6/11追加ここから***************************************/
	/*****************************************UserName.javaを利用************************************/

	//あるEmployee_idの名前と所属部署を取得する処理
	@Override
	public List<UserName> getUserInfomation() throws Exception {
		List<UserName> userList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT employee_id as employee_id,employee.name as name,groups.name as group_name FROM employee JOIN groups ON groups.id = employee.group_id "
					+
					"WHERE NOT EXISTS (SELECT employee_id FROM users " +
					"where employee.employee_id=users.employee_id)";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			//選択してください項目の追加
			UserName username = new UserName();
			username.setEmployee_id("選択してください");
			userList.add(username);

			while (rs.next()) {
				userList.add(mapTogetUserInfomation(rs));
			}
		}
		return userList;
	}

	private UserName mapTogetUserInfomation(ResultSet rs) throws SQLException {
		UserName username = new UserName();

		username.setEmployee_id(rs.getString("employee_id"));
		username.setEmployee_name(rs.getString("name"));
		username.setGroups_name(rs.getString("group_name"));

		return username;
	}

	//Employee_idをすべて取得する
	//ここ変更
	//
	//
	//
	//
	@Override
	public List<String> getAllEmployee_id() throws Exception {
		List<String> Employee_id_List = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT EMPLOYEE_ID FROM USERS";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Employee_id_List.add(rs.getString("employee_id"));

			}
		}
		return Employee_id_List;

	}

	@Override
	public boolean registUser() throws Exception {
		boolean flag = false;
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT COUNT(employee_id) as num FROM employee JOIN groups ON groups.id = employee.group_id "
					+
					"WHERE NOT EXISTS (SELECT employee_id FROM users " +
					"where employee.employee_id=users.employee_id)";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				if (rs.getInt("num") == 0) {
					flag = true;
				}

			}
		}
		return flag;

	}

	//************************************0613追加****************************************************************
	//イベント用
	@Override
	public List<String> findtypesidAll() throws Exception {
		List<String> typesidList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT id,name FROM groups;";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				typesidList.add(rs.getString("id"));

			}

		}

		return typesidList;
	}

	@Override
	public List<String> findtypesAll() throws Exception {
		List<String> typesList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT id,name FROM groups;";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				typesList.add(rs.getString("name"));

			}

		}

		return typesList;
	}

	@Override
	public List<String> listLogin_id() throws Exception {

		List <String>usersList=new ArrayList<>();
		try (Connection con = ds.getConnection()) {

			String sql = "SELECT LOGIN_ID FROM users;";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				usersList.add(rs.getString("LOGIN_ID"));

			}

		}
		return usersList;
	}

}
