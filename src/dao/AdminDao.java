/**
 *
 */
package dao;

import java.util.List;

import domain.Admin;

/**
* The copyright to the computer program(s) herein is the property of AMS Create,inc.
*  Chapter05.データベースプログラミング
*  Section11.ユーザ認証
*
*
*  練習11 ユーザ認証練習
*
* AdminDao Interface
*
* @author trainee
* @version 1.0
*/
public interface AdminDao {

	public boolean findByLoginId(String loginId);

	public List<Admin> findAll() throws Exception;

	public Admin findById(Integer id) throws Exception;

	public void insert(Admin admin) throws Exception;

	public void update(Admin admin) throws Exception;

	public void delete(Admin admin) throws Exception;

	public Admin findByLoginIdAndLoginPass(String loginId, String loginPass) throws Exception;
}
