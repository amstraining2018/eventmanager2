package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import domain.Event;
import domain.Group_kotaki;
import domain.Room;

public class EventDaoImpl implements EventDao {

	private DataSource ds;
	private static final int ROW_COUNT = 5;

	public EventDaoImpl(DataSource ds) {

		this.ds = ds;
	}

	/******************************************************************************************************************/
	/***************************雲居***********************************************************************************/
	/******************************************************************************************************************/

	@Override
	public int findById(String name) throws Exception {
		int groupId = 0;
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT * FROM groups WHERE name=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();

			rs.next();
			groupId = rs.getInt("ID");

		}
		return groupId;
	}

	@Override
	public int getEventManageAllPageNumber() throws Exception {
		int allPageNumber = 1;

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT count(id) AS count FROM events";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				allPageNumber = (rs.getInt("count") - 1) / ROW_COUNT + 1;
			}
		}
		return allPageNumber;
	}

	@Override

	public int getEventManageAllPageNumber(int a, int b) throws Exception {
		int allPageNumber = 1;
		String where = null;
		if (a == 0) {
			if (b == 0) {
				//a==0,b==0
				where = " ; ";
			} else {
				//a==0,b!=0
				where = " WHERE events.room_id= " + b;
			}

		} else if (b == 0) {
			//a!=0,b==0
			where = " WHERE groups.id= " + a;
		} else {
			//a!=0,b!=0
			where = " WHERE groups.id= " + a + " AND events.room_id= " + b;
		}

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT count(events.id) AS count FROM (events JOIN groups ON events.group_id = groups.id) "
					+ "JOIN rooms ON rooms.id = events.room_id "
					+ where;
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				allPageNumber = (rs.getInt("count") - 1) / ROW_COUNT + 1;
			}
		}
		return allPageNumber;
	}

	////////////////////////
	///0605 前田により変更///
	////////////////////////

	@Override
	public List<Event> findForEventManage(int PageNumber) throws Exception {
		List<Event> eventList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {
			//EVENTSのID・TITLE・START・PLACE・DETAILとGROUPSのNAMEを取得し、
			//EVENTSとGROUPSの表を合体し、
			//EVENTS.GROUP_IDにGROUPS.IDを代入し、
			//EVENTS.IDを昇順し、
			//5件だけ限定する。

			String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
					+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id "
					//					+ "WHERE groups.id=? AND events.room_id=? "
					+ "ORDER BY events.start ASC "
					+ "LIMIT " + (PageNumber - 1) * +ROW_COUNT + " , " + ROW_COUNT;

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				//現在の位置から順方向に1行移動します。ResultSetのrsは、
				//初期状態では最初の行の前に位置付けられています。
				//mapToEvent_kumoiで取得する内容は決められている。

				eventList.add(mapToEvent_kumoi(rs));
			}
		}

		return eventList;
	}

	private Event mapToEvent_kumoi(ResultSet rs) throws SQLException {
		Event event = new Event();
		event.setId((Integer) rs.getObject("EVENTS.ID"));
		event.setTitle(rs.getString("TITLE"));
		event.setStart((Date) rs.getObject("START"));
		event.setPlace(rs.getString("place"));
		event.setDetail(rs.getString("DETAIL"));
		event.setGroup_name(rs.getString("GROUPS.NAME"));
		return event;
	}

	/******************************************************************************************************************/
	/***************************前田***********************************************************************************/
	/******************************************************************************************************************/

	////////////////////////
	///0605 前田により変更///
	////////////////////////
	/*
	 * 本日のイベント情報を取得します
	 * EventDao#findTodaysEvent()
	 */
	@Override
	public List<Event> findTodaysEvent(int PageNumber) throws Exception {

		//domainにあるEventを型にしてArrayListを作成
		List<Event> eventList = new ArrayList<>();

		//[接続方法]コンストラクタで引き渡されたデータソース（group2db）でコネクションオブジェクトを取得する
		try (Connection con = ds.getConnection()) {

			String sql = "SELECT events.id,events.title, events.start, rooms.name as place, groups.name " +
					"FROM (events JOIN groups ON events.group_id = groups.id) " +
					"JOIN rooms ON  rooms.id = events.room_id " +
					"AND DATE(events.start) = current_date "
					+ "LIMIT " + (PageNumber - 1) * +ROW_COUNT + " , " + ROW_COUNT;

			/* SQL文の説明
			 *
			 * 本日のイベント情報であるタイトル、スタートとイベントのidを取得
			 * 同じ部署であることを条件にして部署名も取得
			 * DATE型でイベント開始時間をキャストすることで今日の日と比較ができるようになる
			 * LIMITでは受け取ったページ数（今いるページ）を引数にして、5件分だけ取得する
			 *
			 * 例:1ページあたりに5件表示したい
			 * 1ページ目であれば、1～5件目が表示される
			 * 2ページ目であれば、6～10件目が表示される
			 * 以上より(PageNumber-1)*5+5が求められる
			 *
			 */

			//sql分を引数にして、接続先と関連付けたPreparedStatement型stmtを生成
			PreparedStatement stmt = con.prepareStatement(sql);
			//実行したSQLの結果をResultSet型rsに格納
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				//取得された結果で次の行がなくなるまで
				//mapToTodaysEventメソッドでeventListに格納
				eventList.add(mapToTodaysEvent(rs));

			}

		}

		//配列イベントリストを呼び出し元に返す
		return eventList;

	}

	private Event mapToTodaysEvent(ResultSet rs) throws SQLException {

		Event event = new Event();

		//結果からゲッターを使い特定のものだけEventのセッターでセットする（オブジェクト型ならキャスト）
		event.setId((Integer) rs.getObject("id"));
		event.setTitle(rs.getString("title"));
		event.setStart((Date) rs.getObject("START"));
		event.setPlace(rs.getString("place"));
		event.setGroup_name(rs.getString("name"));

		return event;

	}

	/*
	 * 総ページ数カウンター
	 */
	@Override
	public int getPageNumbers() throws Exception {
		int PageNumbers = 1;

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT COUNT(events.id) as count "
					+ " FROM events JOIN groups "
					+ "ON events.group_id = groups.id "
					+ "WHERE date(events.start) = current_date";

			/* SQL文の説明
			 * 今日の日と一致するイベントの総数を取得するものでcoutという名前でカウントしている
			 */
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				PageNumbers = (rs.getInt("count") - 1) / 5 + 1;
				//カウントした行数を5で割り1加算することにより5県ごとのページ数を表示
			}
		}
		return PageNumbers;
	}

	/******************************************************************************************************************/
	/***************************前田ここまで****************************************************************************/
	/******************************************************************************************************************/
	/*************************菱田ここから******************************************************************************/
	/******************************************************************************************************************/

	/*
	 * 0527菱田くん          イベント詳細タイトル～登録者表示用
	 */
	@Override
	public List<Event> findDetail(int id) throws Exception {
		List<Event> eventList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT title,start,end,rooms.name as place,groups.name,detail,employee.name " +
					"FROM (((events JOIN groups on events.group_id=groups.id) " +
					"JOIN users ON users.id = events.registered_by) " +
					"JOIN rooms ON events.room_id=rooms.id) " +
					"JOIN employee ON employee.employee_id=users.employee_id " +
					"WHERE events.id= ?;";

			/*
			 *  SQL文説明
			 *  詳細に表示されるものを取得する
			 *  取得したidを条件にして、(eventsとgroups)とusersの三つをjoin
			 *  usersをusers1にした理由は不明    （区別するため？）
			 *  登録者が一致しているかどうかを222行目で    (名前取得)
			 *  部署が一致しているものをその上の行で条件にしている   （部署名取得）
			 */

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				eventList.add(mapToEvent(rs));
			}
		}
		return eventList;
	}

	private Event mapToEvent(ResultSet rs) throws SQLException {
		Event event = new Event();
		event.setTitle(rs.getString("events.title"));
		event.setStart((Date) rs.getObject("events.start"));
		event.setEnd((Date) rs.getObject("events.end"));
		event.setPlace(rs.getString("place"));
		event.setGroup_name(rs.getString("groups.name"));
		event.setDetail(rs.getString("events.detail"));
		event.setRegistered_name(rs.getString("employee.name"));
		return event;
	}

	/*
	 * 0527菱田君             参加しないボタン
	 * @see dao.EventDao#sankasinaiButton(int, int)
	 */
	@Override
	public boolean sankasinaiButton(int user_id, int event_id) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		boolean flg = false;

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT * FROM attends WHERE user_id=? and event_id=?;";
			/*
			 * SQL文説明
			 * user_idとevent_idを引数にしてattendsから情報を取得
			 * もしその人がイベントに参加していれば一行だけ現れる
			 */

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, user_id);
			stmt.setInt(2, event_id);
			ResultSet rs = stmt.executeQuery();

			//行があるかないか
			while (rs.next()) {
				flg = true;
			}

		}
		return flg;
	}

	/*
	 * 0528菱田君
	 * REGISTERED_BY
	 */
	@Override
	public int getREGISTERED_BY(int event_id) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		int REGISTERED_BY = 0;

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT events.registered_by FROM events WHERE events.id=?;";

			/*
			 * SQL文説明
			 * idを引数にしてREGISTERED_BYを取得
			 *
			 */

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, event_id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				REGISTERED_BY = rs.getInt("REGISTERED_BY");

			}
			return REGISTERED_BY;
		}
	}

	@Override
	public void insert(Event event, int id) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		try (Connection conn = ds.getConnection()) {
			String sql = "INSERT INTO events (title,start,end,room_id,group_id,detail,registered_by,created) "
					+ "VALUES(?,?,?,?,?,?,?,now())";
			//			String sql1 = "INSERT INTO groups (name) values(?)";

			PreparedStatement stmt = conn.prepareStatement(sql);
			//			PreparedStatement stmt1 = conn.prepareStatement(sql1);

			stmt.setString(1, event.getTitle());
			stmt.setObject(2, event.getStart());
			stmt.setObject(3, event.getEnd());
			stmt.setObject(4, event.getRooms_id());
			stmt.setString(6, event.getDetail());
			stmt.setObject(7, id, Types.INTEGER);
			stmt.setObject(5, event.getGroup_id(), Types.INTEGER);
			stmt.executeUpdate();
		}

	}

	/******************************************************************************************************************/
	/******************************************菱田君ここまで***********************************************************/
	/******************************************************************************************************************/
	/******************************************************************************************************************/
	/******************************************井手君ここから***********************************************************/
	/******************************************************************************************************************/

	@Override
	public void delete(Integer integer) throws Exception {
		// TODO 自動生成されたメソッド・スタブ

		try (Connection con = ds.getConnection()) {
			//武智
			//削除のsql文をString型のsqlに格納
			//ID=integerのintegerにはEventDeleteServletで保持しているevent_idが入る
			//これにより、特定のidだけを削除することが可能
			String sql = "DELETE FROM events WHERE  id= ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, integer);
			stmt.executeUpdate();
		}

	}

	/******************************************************************************************************************/
	/******************************************井手君ここまで***********************************************************/
	/******************************************************************************************************************/
	/******************************************************************************************************************/
	/******************************************小滝君ここから***********************************************************/
	/******************************************************************************************************************/

	@Override
	public Event findByIdForEventEdit(int id) throws Exception {
		Event event = new Event();

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT  events.title AS title, events.start AS start, events.end AS end, rooms.name AS place, events.group_id AS group_id, groups.name AS group_name, events.detail AS detail "
					+
					"FROM (events JOIN groups  ON events.group_id = groups.id ) JOIN rooms ON events.room_id=rooms.id "
					+ "WHERE events.id = " + id + ";";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				event.setTitle(rs.getString("title"));
				event.setStart((Date) rs.getObject("start"));
				event.setEnd((Date) rs.getObject("end"));
				event.setPlace(rs.getString("place"));
				event.setGroup_id(rs.getInt("group_id"));
				event.setGroup_name(rs.getString("group_name"));
				event.setDetail(rs.getString("detail"));
			}
		}

		return event;
	}

	@Override
	public List<Group_kotaki> findAllGroupName() throws Exception {
		List<Group_kotaki> groups = new ArrayList<>();

		try (Connection con = ds.getConnection()) {

			String sql = "SELECT * FROM groups";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Group_kotaki group = new Group_kotaki();
				group.setId(rs.getInt("id"));
				group.setGroup_name(rs.getString("name"));
				groups.add(group);
			}
		}

		return groups;
	}

	//5月28日に小滝が変更
	@Override
	public void updateForEventEdit(String event_id, String[][] update) throws Exception {

		try (Connection con = ds.getConnection()) {

			for (String[] element : update) {
				if (!element[1].equals("")) {

					String sql = null;

					if (element[2].equals("int")) {
						sql = "UPDATE events SET " + element[0] + "=" + element[1] + " WHERE id=" + event_id + ";";
					} else {
						sql = "UPDATE events SET " + element[0] + "=\"" + element[1] + "\" WHERE id=" + event_id + ";";
					}

					PreparedStatement stmt = con.prepareStatement(sql);

					stmt.executeUpdate();
				}
			}

		}

	}

	/******************************************************************************************************************/
	/******************************************小滝君ここまで***********************************************************/
	/******************************************************************************************************************/

	/******************************************************************************************************************/
	/******************************************************************************************************************/
	/******************************************未使用******************************************************************/
	/******************************************************************************************************************/
	@Override
	public void update(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public Event findById(Integer id) throws Exception {
		return null;
	}

	@Override
	public void insert(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void insertRoom(Room room) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		try (Connection conn = ds.getConnection()) {
			String sql = "INSERT INTO rooms (name,capacity,mic,board,projector,close) "
					+ "VALUES(?,?,?,?,?,?)";
			//			String sql1 = "INSERT INTO groups (name) values(?)";

			PreparedStatement stmt = conn.prepareStatement(sql);
			//			PreparedStatement stmt1 = conn.prepareStatement(sql1);

			stmt.setString(1, room.getName());
			stmt.setObject(2, room.getCapacity());
			stmt.setObject(3, room.getMic());
			stmt.setObject(4, room.getBoard());
			stmt.setObject(5, room.getProjector());
			stmt.setObject(6, room.getTime());
			stmt.executeUpdate();
		}

	}

	@Override
	public List<Event> searchForEventManage(int PageNumber, int group_id, int room_id) throws Exception {
		List<Event> eventList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {
			//EVENTSのID・TITLE・START・PLACE・DETAILとGROUPSのNAMEを取得し、
			//EVENTSとGROUPSの表を合体し、
			//EVENTS.GROUP_IDにGROUPS.IDを代入し、
			//EVENTS.IDを昇順し、
			//5件だけ限定する。

			//switch文で分岐
			//			String a = null;
			//			groupId==0&&roomId==0
			//groupId!=0,roomId==0
			//groupId==0,roomId!=0
			//groupId!=0,roomId==0
			//			int sentaku = 0;
			//
			//			switch (sentaku) {
			//
			//			case 0:
			//				a = "==";
			//				break;
			//
			//			case 1:
			//				a = "=!";
			//				break;
			//
			//			}

			if (group_id == 0) {
				if (room_id == 0) {
					//group_id==0&&room_id==0
					String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
							+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id "
							+ "ORDER BY events.start ASC "
							+ "LIMIT " + (PageNumber - 1) * +ROW_COUNT + " , " + ROW_COUNT;

					PreparedStatement stmt = con.prepareStatement(sql);

					ResultSet rs = stmt.executeQuery();

					while (rs.next()) {
						//現在の位置から順方向に1行移動します。ResultSetのrsは、
						//初期状態では最初の行の前に位置付けられています。
						//mapToEvent_kumoiで取得する内容は決められている。

						eventList.add(mapToEvent_kumoi(rs));
					}

				} else {
					//group_id==0&&room_id!=0
					String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
							+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id "
							+ "WHERE  events.room_id=? "
							+ "ORDER BY events.start ASC "
							+ "LIMIT " + (PageNumber - 1) * +ROW_COUNT + " , " + ROW_COUNT;

					PreparedStatement stmt = con.prepareStatement(sql);
					stmt.setObject(1, room_id);

					ResultSet rs = stmt.executeQuery();

					while (rs.next()) {
						//現在の位置から順方向に1行移動します。ResultSetのrsは、
						//初期状態では最初の行の前に位置付けられています。
						//mapToEvent_kumoiで取得する内容は決められている。

						eventList.add(mapToEvent_kumoi(rs));
					}
				}
			} else if (room_id == 0) {
				//group_id!=0&&room_id==0
				String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
						+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id "
						+ "WHERE groups.id=?  "
						+ "ORDER BY events.start ASC "
						+ "LIMIT " + (PageNumber - 1) * +ROW_COUNT + " , " + ROW_COUNT;

				PreparedStatement stmt = con.prepareStatement(sql);
				stmt.setObject(1, group_id);

				ResultSet rs = stmt.executeQuery();

				while (rs.next()) {
					//現在の位置から順方向に1行移動します。ResultSetのrsは、
					//初期状態では最初の行の前に位置付けられています。
					//mapToEvent_kumoiで取得する内容は決められている。

					eventList.add(mapToEvent_kumoi(rs));
				}
			} else {
				//group_id!=0&&room_id!=0
				String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
						+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id "
						+ "WHERE groups.id=? AND events.room_id=? "
						+ "ORDER BY events.start ASC "
						+ "LIMIT " + (PageNumber - 1) * +ROW_COUNT + " , " + ROW_COUNT;

				PreparedStatement stmt = con.prepareStatement(sql);
				stmt.setObject(1, group_id);
				stmt.setObject(2, room_id);
				ResultSet rs = stmt.executeQuery();

				while (rs.next()) {
					//現在の位置から順方向に1行移動します。ResultSetのrsは、
					//初期状態では最初の行の前に位置付けられています。
					//mapToEvent_kumoiで取得する内容は決められている。

					eventList.add(mapToEvent_kumoi(rs));
				}
			}
		}

		return eventList;

	}

	@Override
	public List<Event> findForEventManageAll(int group_id, int room_id) throws Exception {
		List<Event> eventList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {
			if (group_id == 0) {
				if (room_id == 0) {
					//group_id==0&&room_id==0
					String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
							+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id ";

					PreparedStatement stmt = con.prepareStatement(sql);

					ResultSet rs = stmt.executeQuery();

					while (rs.next()) {
						//現在の位置から順方向に1行移動します。ResultSetのrsは、
						//初期状態では最初の行の前に位置付けられています。
						//mapToEvent_kumoiで取得する内容は決められている。

						eventList.add(mapToEvent_kumoi(rs));
					}

				} else {
					//group_id==0&&room_id!=0
					String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
							+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id "
							+ "WHERE  events.room_id=? ";

					PreparedStatement stmt = con.prepareStatement(sql);
					stmt.setObject(1, room_id);

					ResultSet rs = stmt.executeQuery();

					while (rs.next()) {
						//現在の位置から順方向に1行移動します。ResultSetのrsは、
						//初期状態では最初の行の前に位置付けられています。
						//mapToEvent_kumoiで取得する内容は決められている。

						eventList.add(mapToEvent_kumoi(rs));
					}
				}
			} else if (room_id == 0) {
				//group_id!=0&&room_id==0
				String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
						+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id "
						+ "WHERE groups.id=?  ";

				PreparedStatement stmt = con.prepareStatement(sql);
				stmt.setObject(1, group_id);

				ResultSet rs = stmt.executeQuery();

				while (rs.next()) {
					//現在の位置から順方向に1行移動します。ResultSetのrsは、
					//初期状態では最初の行の前に位置付けられています。
					//mapToEvent_kumoiで取得する内容は決められている。

					eventList.add(mapToEvent_kumoi(rs));
				}
			} else {
				//group_id!=0&&room_id!=0
				String sql = "SELECT events.id,events.title,events.start,rooms.name as place,groups.name,events.detail "
						+ "FROM (events JOIN groups ON events.group_id = groups.id) JOIN rooms ON rooms.id = events.room_id "
						+ "WHERE groups.id=? AND events.room_id=? ";

				PreparedStatement stmt = con.prepareStatement(sql);
				stmt.setObject(1, group_id);
				stmt.setObject(2, room_id);
				ResultSet rs = stmt.executeQuery();

				while (rs.next()) {
					//現在の位置から順方向に1行移動します。ResultSetのrsは、
					//初期状態では最初の行の前に位置付けられています。
					//mapToEvent_kumoiで取得する内容は決められている。

					eventList.add(mapToEvent_kumoi(rs));
				}
			}
		}

		return eventList;

	}

}
