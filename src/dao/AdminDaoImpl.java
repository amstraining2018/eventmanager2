/**
 *
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.mindrot.jbcrypt.BCrypt;

import domain.Admin;

/**
* The copyright to the computer program(s) herein is the property of AMS Create,inc.
*  Chapter05.データベースプログラミング
*  Section11.ユーザ認証
*
*
*  練習11 ユーザ認証練習
*
* AdminDao Interface の実装クラス
*
* @author trainee
* @version 1.0
*/
public class AdminDaoImpl implements AdminDao {

	private DataSource ds;

	public AdminDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/* (非 Javadoc)
	 * @see dao.AdminDao#findAll()
	 */
	@Override
	public List<Admin> findAll() throws Exception {
		return null;
	}

	//5.30に徐が追加
	public boolean findByLoginId(String loginId) {

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT * "
					+ " FROM users WHERE login_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();
			if (false == rs.next()) {

				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;

	}

	/**
	 *
	 * ResultSetをAdminクラスにセットします
	 * @return ResultSet rs
	 */
	private Admin mapToAdmin(ResultSet rs) throws SQLException {
		Admin Admin = new Admin();
		Admin.setId((Integer) rs.getObject("id"));
		Admin.setLoginId(rs.getString("LOGIN_ID"));
		Admin.setLoginPass(rs.getString("LOGIN_PASS"));
		Admin.setLoginName(rs.getString("employee.NAME"));
		Admin.setUserType((Integer) rs.getObject("TYPE_ID"));

		return Admin;
	}

	/* (非 Javadoc)
	 * @see dao.AdminDao#findById(java.lang.Integer)
	 */
	@Override
	public Admin findById(Integer id) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	/**
	 *  Adminをテーブルに追加します
	 *  @param Adminクラス
	 *
	 */
	@Override
	public void insert(Admin Admin) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "INSERT INTO admins"
					+ " (id,login_id,login_pass) "
					+ "VALUES(?,?,?)";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, Admin.getId(), Types.INTEGER);
			stmt.setString(2, Admin.getLoginId());
			stmt.setString(3, Admin.getLoginPass());

			stmt.executeUpdate();
		}
	}

	/**
	 *  ユーザIDをキーにパスワードを抽出します
	 *  パスワードは暗号化されており、checkpwメソッドによって
	 *  プレーンテキストのパスワードが以前にハッシュされたものと
	 *  一致するか確認しています
	 *
	 *  @param String loginId
	 *          String loginPass
	 *
	 */
	@Override
	public Admin findByLoginIdAndLoginPass(String loginId, String loginPass) throws Exception {
		Admin admin = null;

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT * FROM users  join employee ON users.employee_id=employee.employee_id " +
					"WHERE login_id= ? ";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {

				if (BCrypt.checkpw(loginPass, rs.getString("LOGIN_PASS"))) {
					admin = mapToAdmin(rs);
				}
			}
		}
		return admin;
	}

	/* (非 Javadoc)
	 * @see dao.AdminDao#update(domain.Admin)
	 */
	@Override
	public void update(Admin Admin) throws Exception {
		// TODO 自動生成されたメソッド・スタブ

	}

	/* (非 Javadoc)
	 * @see dao.AdminDao#delete(domain.Admin)
	 */
	@Override
	public void delete(Admin Admin) throws Exception {
		// TODO 自動生成されたメソッド・スタブ

	}

}
