package dao;

import java.util.List;

import domain.Attends;

public interface AttendsDao {
	//0524追加 attendsから
	public List<Attends> getAttends(int id) throws Exception;

	//0527菱田君
	public void insertAttends(Attends attends) throws Exception;

	public void deleteAttends(Attends attends) throws Exception;

}
