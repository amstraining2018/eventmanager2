package dao;

import java.util.List;

import domain.Event;
import domain.Group_kotaki;
import domain.Room;

public interface EventDao {

	///////////////////////////////////雲井君///////////////////////////////////////////////////////////////////////////

	public int findById(String name) throws Exception;

	public int getEventManageAllPageNumber() throws Exception;

	/////////////////
	//前田により変更//
	/////////////////
	public List<Event> findForEventManage(int PageNumber) throws Exception;

	///////////////////////////////////前田/////////////////////////////////////////////////////////////////////////////

	/////////////////
	//前田により変更//
	/////////////////
	public List<Event> findTodaysEvent(int PageNumber) throws Exception;

	public int getPageNumbers() throws Exception;

	///////////////////////////////////菱田君///////////////////////////////////////////////////////////////////////////

	/////////////////
	//前田により変更//
	/////////////////
	public List<Event> findDetail(int id) throws Exception;

	public boolean sankasinaiButton(int user_id, int event_id) throws Exception;

	public int getREGISTERED_BY(int event_id) throws Exception;

	/////////////////
	//前田により変更//
	/////////////////////////////////////
	//room.idで登録するようにしてください//
	/////////////////////////////////////
	public void insert(Event event, int id) throws Exception;

	///////////////////////////////////井手君///////////////////////////////////////////////////////////////////////////
	public void delete(Integer integer) throws Exception;

	///////////////////////////////////小滝君///////////////////////////////////////////////////////////////////////////

	/////////////////
	//前田により変更//
	/////////////////
	public Event findByIdForEventEdit(int id) throws Exception;

	public List<Group_kotaki> findAllGroupName() throws Exception;

	public void updateForEventEdit(String event_id, String[][] update) throws Exception;

	//////////////////////////////////長島作成//////////////////////////////////////////////////////////////////////////
	public void insertRoom(Room room) throws Exception;

	public List<Event> searchForEventManage(int PageNumber, int group_id, int room_id) throws Exception;

	///////////////////////////////////未使用///////////////////////////////////////////////////////////////////////////
	public void update(Event event) throws Exception;

	public Event findById(Integer id) throws Exception;

	public void insert(Event event) throws Exception;

	public List<Event> findForEventManageAll(int a, int b) throws Exception;

	public int getEventManageAllPageNumber(int a, int b) throws Exception;

}
