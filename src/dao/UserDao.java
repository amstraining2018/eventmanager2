package dao;

import java.util.List;

import domain.User;
import domain.UserName;

public interface UserDao {

	//0527菱田
	public int findIdByLoginId(String login_id) throws Exception;

	//0528菱田
	public List<User> findByAttendsId(int id) throws Exception;

	//0528前田
	public List<User> getLoginInfo(String login_id) throws Exception;

	//5月23日に小滝が追加
	public int getUserManageAllPageNumber() throws Exception;

	//5月23日に小滝が追加
	public List<User> findForUserManage(int PageNumber) throws Exception;

	// 徐さんが追加したもの
	public List<String> findtypesAll() throws Exception;

	// 徐さんが追加したもの
	public List<String> findtypesidAll() throws Exception;

	// 徐さん用追加したもの(0529)
	public void delete(int id) throws Exception;

	public User findById_jyo(int id) throws Exception;

	public void insert(User user) throws Exception;

	public void update(User user) throws Exception;

	public void insertUser(User user) throws Exception;

	//**********************0611追加********************************************//

	//Employee_idに応じて情報を表示
	public List<UserName> getUserInfomation() throws Exception;

	//Employee_idをすべて表示
	public List<String> getAllEmployee_id() throws Exception;

	//ユーザー登録ボタンでいけるかどうか（UserManagse用）
	public boolean registUser() throws Exception;

	public void update_pass(User user) throws Exception;

	//**********************0618井出追加********************************************//

	public List<String> listLogin_id() throws Exception;



}
