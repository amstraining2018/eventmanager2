package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.Room;

public class RoomDaoImpl implements RoomDao{

	private DataSource ds;

	public RoomDaoImpl(DataSource ds) {

		this.ds = ds;
	}

	/******************************************************************************************************************/
	/******************************************追加分******************************************************************/
	///////////////////////////////////////イベント登録用////////////////////////////////////////////////////////////////
	@Override
	public List<String> getRooms_id() throws Exception {
		List<String> idList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {

			String sql ="SELECT id FROM ROOMS ";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				idList.add(rs.getString("id"));
			}

		}

		return idList;
	}

	@Override
	public List<String> getRooms_name() throws Exception {
		List<String> nameList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {

			String sql ="SELECT name FROM ROOMS ";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				nameList.add(rs.getString("name"));
			}

		}

		return nameList;
	}
	///////////////////////////////////////イベント編集用////////////////////////////////////////////////////////////////
	@Override
	public List<Room> getRooms_nameAndRooms_id() throws Exception {
		List<Room> roomList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {

			String sql ="SELECT id,name FROM ROOMS ";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				roomList.add(mapToRoomInfo(rs));
			}

		}

		return roomList;
	}
	private Room mapToRoomInfo(ResultSet rs) throws SQLException {
		Room room = new Room();
		room.setId((Integer) rs.getObject("id"));
		room.setName(rs.getString("name"));
		return room;
	}

	@Override
	public int findByroom_Id(String name) throws Exception {
		int room_id = 0;
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT id FROM rooms WHERE name=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();

			rs.next();
			room_id = rs.getInt("id");

		}
		return room_id;
	}



}
