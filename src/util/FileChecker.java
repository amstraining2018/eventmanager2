package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class FileChecker {

	private FileChecker() {
	}

	private final static String DataFormat ="yyyy/M/d HH:mm";

	/**
	 * S,H,D,Eのいずれかの文字が入っているかチェック
	 * @param alf
	 * アルファベット一文字
	 * @return
	 * SHDEのどれかであった場合はtrue
	 */
	public static boolean checkRowAlf(String alf) {
		boolean flg = false;
		//SHDEのいづれかであればtrueがセットされる。
		if (alf.equals("S") || alf.equals("H") || alf.equals("D") || alf.equals("E")) {
			flg = true;
		}

		return flg;
	}

	/**
	 *時間がフォーマット通りであるか否かチェック
	 * @param tf
	 * 時間を取得
	 * @return
	 * フォーマット化した時間と取得した時間が正しければtrue
	 * @throw
	 * 日付フォーマットとして認識されない場合スロー
	 */
	public static boolean checkTimeFormat(String tf) throws  ParseException {
		boolean flg;
		SimpleDateFormat sdf = new SimpleDateFormat(DataFormat);
		try {
			//フォーマット化した時間と取得した時間を比較
			if (tf.equals(sdf.format(sdf.parse(tf)))) {
				flg = true;
			} else {
				flg = false;
			}
		} catch (ParseException e) {
			throw e;
		}

		return flg;
}
}
