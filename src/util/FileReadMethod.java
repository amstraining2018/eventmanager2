package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import domain.Room;

/**
 *ファイルを読み込むクラスです
 *
 * */
public class FileReadMethod {
	private static final int COL_CNT = 7;
	private static final String ALF_S = "S";
	private static final String ALF_H = "H";
	private static final String ALF_D = "D";
	private static final String ALF_E = "E";
	private static final int CNT1 = 1;
	private static final int CNT2 = 2;
	private static final String SEPARATE = ",";
	public String errmgs;

	/**
	 *ファイルを読み込むメソッドです
	 *パラメータのディレクトリファイルを読み込み
	 *データ行（D）のみを取得してList<Room>に格納します
	 *
	 *@param address
	 *@return List<Room>
	 *
	 * */
	public List<Room> readCsvFile(String address) throws IOException {
		List<Room> roomList = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(
				address))) {
			//ファイルを読み込む
			//			FileReader fr = new FileReader(
			//					address);
			//			BufferedReader br = new BufferedReader(fr);
			//			List<Room> roomList = new ArrayList<Room>();
			//読み込んだファイルを１行ずつ処理する
			String line;

			//
			int cnt = 1;

			while ((line = br.readLine()) != null) {

				if (cnt <= CNT1) {
					String[] firstRow = line.split(SEPARATE);
					if (!(FileChecker.checkRowAlf(firstRow[0]))) {

						errmgs = "CSVファイルに不正があります：S,H,D,Eのいずれかの文字が入っていません";
						break;
					}
					if (!(firstRow[0].equals(ALF_S))) {
						errmgs = "CSVファイルに不正があります：Sが入っていません";
						break;

					}
					try {
						if (!(FileChecker.checkTimeFormat(firstRow[1]))) {
							errmgs = "CSVファイルに不正があります：時間がフォーマット通りではない、またはnullです";
							break;
						}

					} catch (ParseException e) {
						errmgs = "CSVファイルに不正があります：時間がフォーマット通りではない、またはnullです";
						break;
					}
					//時間がフォーマット通りであるか判定

				} else if (cnt == CNT2) {
					String[] column = line.split(SEPARATE);
					if (!(FileChecker.checkRowAlf(column[0]))) {
						errmgs = "CSVファイルに不正があります:2.1";
						break;
					}
					if (!(column[0].equals(ALF_H))) {
						errmgs = "CSVファイルに不正があります:2,2";
						break;
					}
					//ファイルの要素数が6個あるか判定
					if (!(COL_CNT == column.length)) {
						errmgs = "CSVファイルに不正があります：ヘッダの数が要素数と合致していません";
						break;

					}

				} else {

					String[] rooms = line.split(SEPARATE);
					String alf = rooms[0];
					if (!(FileChecker.checkRowAlf(rooms[0]))) {
						errmgs = "CSVファイルに不正があります3.1";
						break;
					} else if (alf.equals(ALF_D)) {
						if (!(COL_CNT == rooms.length)) {
							errmgs = "CSVファイルに不正があります：ヘッダの数が要素数と合致していません";
							break;
						}
						Room room = new Room();

						if (RecordChecker.checkRoomName(rooms[1])) {
							room.setName(rooms[1]);

						} else {
							errmgs = "会議室名が正しく入力されていません";
							break;
						}
						try {
							RecordChecker.checkNumber(rooms[2]);
							room.setCapacity(Integer.parseInt(rooms[2]));
							System.out.println(rooms[2]);

						} catch (NumberFormatException e) {
							errmgs = "数字で入力されていません：capacity";
							break;

						}
						try {
							RecordChecker.checkNumber(rooms[3]);
							room.setMic(Integer.parseInt(rooms[3]));

						} catch (NumberFormatException e) {
							errmgs = "数字で入力されていません：Mic";
							break;

						}
						try {
							RecordChecker.checkNumber(rooms[4]);
							room.setBoard(Integer.parseInt(rooms[4]));

						} catch (NumberFormatException e) {
							errmgs = "数字で入力されていません：board";
							break;

						}
						try {
							RecordChecker.checkNumber(rooms[5]);
							room.setProjector(Integer.parseInt(rooms[5]));

						} catch (NumberFormatException e) {
							errmgs = "数字で入力されていません：projector";
							break;

						}

						try {
							if (RecordChecker.checkTimeFormat(rooms[6])) {
								room.setTime(rooms[6]);
							} else {
								errmgs = "正しい時間が入力されていません";
								break;
							}

						} catch (ParseException e) {
							errmgs = "正しい時間が入力されていません";
							break;
						}

						roomList.add(room);

					} else if (alf.equals(ALF_E)) {
						if (rooms.length != 2) {
							errmgs = "データの数が入力されていません";
							break;
						}
						try {

							RecordChecker.checkNumber(rooms[1]);
							if (!(cnt - 3 == Integer.parseInt(rooms[1]))) {
								errmgs = "CSVファイルに不正があります：データの数が一致しません";
								break;
							}
						} catch (NumberFormatException e) {
							errmgs = "レコードの数字に問題があります";
							break;
						}

					} else {
						errmgs = "CSVファイルに不正があります:DでもEでもありません";
						break;

					}
				}
				cnt++;
			}

		} catch (FileNotFoundException e) {
			errmgs = "ファイルが見つかりませんでした";

		} catch (IOException e) {
			errmgs = "ファイル読み込み時にエラーが発生しました";

		}
		return roomList;
		//ルームの配列を返す
	}

}
