package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
/*
 * 説明	このプログラムはサーブレットから呼び出してください
 *
 * サーブレットに下記を追記することでサーブレットから本クラスを呼び出すことができます
 *
 *  ServletContext context = this.getServletContext();
 * 	String path = context.getRealPath(""/WEB-INF/classes/message_ja.properties"");
 * 	PropertiesGetter p = new PropertiesGetter();
 * 	p.getProperties(path);
 *
 *
 *
 *
 *
 * */

public class PropertiesGetter {
	private static final String ROOM_FILE = "file.input";
	private static final String OUTPUT_FILE = "file.output";
	private static final String ACCOUNT_FILE = "file.ainput";

	public String getProperties(String fileType, String path) throws IOException, NoSuchFileException {

		Path path1 = Paths.get(path);
		String address = null;
		try (BufferedReader reader = Files.newBufferedReader(path1, StandardCharsets.UTF_8)) {
			Properties pr = new Properties();
			pr.load(reader);
			if (fileType.equals("room")) {
				String adress = pr.getProperty(ROOM_FILE);
				System.out.println(adress);
				address = adress;
			} else if (fileType.equals("account")) {
				String adress = pr.getProperty(ACCOUNT_FILE);
				address = adress;

			} else {
				String adress = pr.getProperty(OUTPUT_FILE);
				address = adress;

			}

		} catch (NoSuchFileException e) {
			throw e;

		} catch (IOException ex) {
			throw ex;

		}
		return address;

	}
}
