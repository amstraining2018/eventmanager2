package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;

import domain.User;

public class AccountFileReadMethod {
	private static final int COL_CNT = 5;
	private static final int START_LINE = 1;
	private static final int HEAD_LINE = 2;
	private static final int ANOTHER_LINE = 3;
	private static final int INDEX_0 = 0;
	private static final int INDEX_1 = 1;
	private static final int INDEX_2 = 2;
	private static final int INDEX_3 = 3;
	private static final int INDEX_4 = 4;
	private static final String SPLIT =",";
	private static final String ALF_S = "S";
	private static final String ALF_H = "H";
	private static final String ALF_D = "D";
	private static final String ALF_E = "E";
	public String err ;
	public List<User> readCsvFile(String address) {
		List<User> userList = new ArrayList<>();

		try {
			//ファイルを読み込む
			FileReader fr = new FileReader(address);
			BufferedReader br = new BufferedReader(fr);
			//読み込んだファイルを１行ずつ処理する
			String line;
			int cnt = 1;
			while ((line = br.readLine()) != null) {

				if (cnt == START_LINE) {
					String[] firstRow = line.split(SPLIT);
					if (!(FileChecker.checkRowAlf(firstRow[INDEX_0]))) {
						err="CSVファイルに不正があります：S,H,D,Eのいずれかの文字が入っていませんS";
						break;
					}
					if (!(firstRow[INDEX_0].equals(ALF_S))) {
						err="CSVファイルに不正があります：Sが入っていません";
						break;
					}
					if (!(firstRow.length==2 )) {
						err="CSVファイルに不正があります：時間がフォーマット通りではない、またはnullです";
						//時間がフォーマット通りであるか判定
						break;
					}
					//フォーマット形式が yyyy/M/d hh:mm 以外である場合
					if(!FileChecker.checkTimeFormat(firstRow[INDEX_1])){
						err="日付のフォーマットに誤りがあります";
						break;
					}

				} else if (cnt == HEAD_LINE) {
					String[] column = line.split(SPLIT);
					if (!(FileChecker.checkRowAlf(column[INDEX_0]))) {
						err="CSVファイルに不正があります：S,H,D,Eのいずれかの文字が入っていませんH";
						break;
					}
					if (!(column[INDEX_0].equals(ALF_H))) {
						err="CSVファイルに不正があります：Hが入っていません";
						break;
					}
					//ファイルの項目数正当性判定
					if (!(COL_CNT==column.length)) { //change a bit
						err="CSVファイルに不正があります：ヘッダの数が要素数と合致していません";
						break;
					}

				} else if (cnt >= ANOTHER_LINE) {

					//CSVファイルが３行目以降はデータをAccountに格納してListに追加

					//リストをインスタンス
					//区切り文字SPLITで分割する
					String[] accounts = line.split(SPLIT);
					if (!(FileChecker.checkRowAlf(accounts[INDEX_0]))) {
						err="CSVファイルに不正があります：S,H,D,Eのいずれかの文字が入っていませんD,E";
						break;
					} else if (accounts[INDEX_0].equals(ALF_D)) {

						//ファイルの要素数が4個あるか判定
						if (!(COL_CNT==accounts.length)) {//change a bit
							err="CSVファイルに不正があります：データの数が要素数と合致していません";
							break;
						}

						User user = new User();

						if (RecordChecker.checkEmp(accounts[INDEX_1])) { //
							user.setEmployee_id((accounts[INDEX_1]));


						} else {
							err="社員IDに不正があります";
							break;
						}
						if (RecordChecker.checkAccount(accounts[INDEX_2])) {
							user.setLogin_id(accounts[INDEX_2]);

						} else {
							err="アカウントに不正があります";
							break;

						}

						if (RecordChecker.checkPass(accounts[INDEX_3])) {
							String login_pass = BCrypt.hashpw(accounts[INDEX_3], BCrypt.gensalt());
							user.setLogin_pass(login_pass);

						} else {
							err="パスワードに不正があります";
							break;
						}

						if (RecordChecker.checkAuth(accounts[INDEX_4])) {
							user.setType_id(Integer.parseInt(accounts[INDEX_4]));//権限


						}else {
							err="権限に不正があります";
							break;
						}
							userList.add(user);
					} else if (accounts[INDEX_0].equals(ALF_E)) {
						if(accounts.length!=2) {
							err="レコードが不正です";
							break;
						}
						if (RecordChecker.checkNumber(accounts[INDEX_1])) {
							if(!(cnt-3==Integer.parseInt(accounts[INDEX_1])&&accounts.length==2)) {

								err="CSVファイルに不正があります";
								break;
							}
						}



					}else {
						err="正しいアルファベットが入力されていません";
						break;
					}

				}
				cnt++;
				//終了処理

			}br.close();
		} catch (FileNotFoundException ex) {
			//例外発生時処理
			ex.printStackTrace();
			err="ファイルが存在しません";
		}catch(IOException e){
			e.printStackTrace();

		} catch (NullPointerException ex) {
			//例外発生時処理
			ex.printStackTrace();
			err="";
		}catch(ParseException e){
			err="CSVファイルに不正があります：時間がフォーマット通りではない、またはnullです";
		}catch(NumberFormatException e){
			err="レコードに数字が入力されていません";
		}

		return userList;

	}
}

//				リストにaccountをaddする

//				System.out.println(fruits);
//				//分割した文字を画面出力する
//				//				while (token.hasMoreTokens()) {
//				//					System.out.print(token.nextToken() + SPLIT);
//				System.out.println(line);
//			System.out.println("");
//				System.out.println("**********");
