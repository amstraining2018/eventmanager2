package util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import domain.Event;

public class EventOutPut {
	public static String path;

	private EventOutPut() {//引数なしコンストラクタ
	}

	//ハードコードにする
	private static final String START = "s,";
	private static final String HEADER = "h,";
	private static final String DATE = "D,";
	private static final String END = "e,";
	private static final String POINT = ",";
	private static final String POINTS = ",,,";
	private static final String HEADERS = "タイトル,年月日・開始時間,会議室,対象部署,参加者数";
	private static final SimpleDateFormat d1 = new SimpleDateFormat("yyyyMMdd_HHmm");
	private static final SimpleDateFormat d3 = new SimpleDateFormat("yyyy/MM/dd HH:mm");

	//パラメーターのリストを出力する
	public static String outPutEvent(List<Event> eventList, String address) {
		//msgの初期値
		String msg = "fail";
		//データ型ｄをインスタンス
		Date d = new Date();
		//ｄの中にフォーマットを格納し変数名の変更
		String d2 = d1.format(d);
		String d4 = d3.format(d);

		try (PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter(address + "\\" + d2 + ".csv", false)))) {
			path = address + "\\" + d2 + ".csv";
			p.print(START);// ヘッダーを指定する
			p.print(d4);
			p.println(POINTS);
			p.print(HEADER);
			p.println(HEADERS);
			int cnt = 0;

			for (Event e : eventList) {// 内容をセットする
				p.print(DATE);//Eventのゲッターで値をとってくる
				p.print(e.getTitle());
				p.print(POINT);
				p.print(d3.format(e.getStart()));
				p.print(POINT);
				p.print(e.getPlace());
				p.print(POINT);
				p.print(e.getGroup_name());
				p.print(POINT);
				p.print(e.getAttendNum());
				p.println(); // 改行
				cnt++;
			}
			p.print(END);
			p.print(cnt);
			p.print(POINTS);
			//成功したらmsgにsuccessが入る
			msg = "success";
			//例外の場合はmsgにfailが入る
		} catch (IOException ex) {
			return msg;
			//nullの場合はmsgにfailが入る
		} catch (NullPointerException ex) {
			return msg;
		}
		//成功したらmsgにsuccessが入り、サーブレットにもどる
		return msg;
	}
}
