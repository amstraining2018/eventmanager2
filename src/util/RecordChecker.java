package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class RecordChecker {

	/**
	 * すべてのメソッドがstaticであるため、明示的にprivateのコンストラクタを定義。
	 */
	private RecordChecker() {

	}

	/**
	 * 名前(name)を取得。
	 * 50文字未満なら、true
	 * 50文字以上なら、false
	 * @param name
	 * @return
	 */
	public static boolean checkRoomName(String name) {
		boolean flg = false;
		if (name.length() < 50) {
			flg = true;
		}

		return flg;
	}

	/**
	 * データ数(num)を取得
	 * 数字のみの文字列なら、true
	 * 数字以外が含まれている文字列なら、NumberFormatException
	 * @param num
	 * @return boolean flg
	 * @throws NumberFormatException
	 */
	public static boolean checkNumber(String num) throws NumberFormatException {
		boolean flg = false;
		try {
			Integer.parseInt(num);
			flg = true;
		} catch (NumberFormatException e) {
			flg = false;
			throw e;
		}
		return flg;
	}

	/**
	 *
	 * 時間(tf)を取得
	 * フォーマット化した時間と取得した時間が正しければtrue
	 * 数字以外が含まれていた場合などは、ParseException
	 * フォーマットが異なる時間の場合、false
	 * @param tf
	 * @return boolean flg
	 * @throws ParseException
	 */

	public static boolean checkTimeFormat(String tf) throws ParseException {
		boolean flg = false;
		Date time = null;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		try {
			time = sdf.parse(tf);
			if (tf.equals(sdf.format(sdf.parse(tf)))) {
				return true;
			}
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			throw e;
		}
		System.out.println(time);

		return flg;
	}

	/**
	 * 社員Id(employeeId)を取得
	 * 10文字未満なら、true
	 * 10文字以上なら、false
	 * @param employeeId
	 * @return
	 */
	public static boolean checkEmp(String employeeId) {
		boolean flg = false;
		if (employeeId.length() < 10) {
			flg = true;
		}
		return flg;
	}

	/**
	 * ログインId(account)を取得
	 * 50文字未満なら、true
	 * 50文字以上なら、false
	 * @param account
	 * @return boolean flg
	 */
	public static boolean checkAccount(String account) {
		boolean flg = false;
		if (account.length() < 50) {
			flg = true;
		}
		return flg;
	}

	/**
	 * パスワード(password)を取得
	 * 空文字ではなく、なおかつ半角英数字の8文字以上12文字以下なら、true
	 * そうでないときに、false
	 * @param password
	 * @return boolean flg
	 */
	public static boolean checkPass(String password) {
		boolean flg = false;
		//半角英字数字8文字以上12文字未満かどうかを判定
		String pattern = "^([a-zA-Z0-9]{8,12})$";

		Pattern p = Pattern.compile(pattern);

		if (!password.isEmpty() && p.matcher(password).find()) {
			flg = true;
		}
		return flg;
	}

	/**
	 * タイプId(authority)を取得
	 * 11文字未満なら、true
	 * 11文字以上なら、false
	 * @param authority
	 * @return boolean flg
	 */
	public static boolean checkAuth(String authority) {
		boolean flg = false;
		if (authority.length() < 11) {
			flg = true;
		}
		return flg;
	}

}
