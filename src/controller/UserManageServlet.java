package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;

/**
 * Servlet implementation class UserManageServlet
 */
@WebServlet("/UserManageServlet")
public class UserManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserManageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserDao userDao = DaoFactory.createUserDao();

		try {

			//***********************ユーザー登録ボタン****************************//

			//登録ﾎﾞﾀﾝ表示用
			String flg = "false";
			request.setAttribute("flg", flg);

			//***表示するページ情報のためのリクエスト取得して送る***
			String pageNumAsString = request.getParameter("nowPageNumber");
			//String型のpageNumAsStringの中に
			int pageNumAsInt = 1;
			//int型のpageNumAsIntを定義し、その中に1をいれている
			if (pageNumAsString == null) {
				request.setAttribute("nowPageNumber", pageNumAsInt);
				//pageNumAsStringの値がnullの場合は
				//pageNumAsIntに1をいれていく。
			} else {
				pageNumAsInt = Integer.parseInt(pageNumAsString);
				request.setAttribute("nowPageNumber", pageNumAsInt);
				//上の条件（pageNumAsStringがnullだった場合）ではない場合は
				//pageNumAsIntに1をいれる。
			}

			//****************************************************************

			//DaoFactoryのcreateUserDaoの実行する情報(new UserDaoImpl(getDataSource())を呼びだすメソッド)を
			//"userDao"に代入する。
			List<User> userList = userDao.findForUserManage(pageNumAsInt);
			//userDaoのfindForUserManageの実行結果を"userList"に格納していく。
			request.setAttribute("userList", userList);
			//userListにある値を"userList"という変数名で送れるようにする。
			//****************************************************************

			//***全ページ数を送る***
			int allPageNumber = userDao.getUserManageAllPageNumber();
			//userDao.getUserManageAllPageNumberで実行したメソッドをint型で"allPageNumber"に代入する。
			request.setAttribute("allPageNumber", allPageNumber);
			//allPageNumberにある値を"allPageNumber"という変数名で送れるようにする。
			//************************

			request.getRequestDispatcher("WEB-INF/view/userManage.jsp").forward(request, response);

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
