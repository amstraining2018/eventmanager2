package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendsDao;
import dao.DaoFactory;
import domain.Attends;

/**
 * Servlet implementation class EventAttendsDeleteServlet
 */
@WebServlet("/EventAttendsDeleteServlet")
public class EventAttendsDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventAttendsDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//eventDetail.jspで保持していたevent_idをrequest.getParameterを使い取得
		//Integer.parseIntを用いて、int型のevent_idに格納
		int event_id = Integer.parseInt(request.getParameter("event_id"));

		//eventDetail.jspで保持していたuser_idをrequest.getParameterを使い取得
		//Integer.parseIntを用いて、int型のuser_idに格納
		int user_id = Integer.parseInt(request.getParameter("user_id"));

		Attends attends = new Attends();

		//Attends.javaで定義しているevent_idとuser_idに38,42行目にあるint型の変数をセット
		attends.setEvent_id(event_id);
		attends.setUser_id(user_id);
		try {
			AttendsDao attendsDao = DaoFactory.createAttendsDao();

			//AttendsDaoImplにあるdeleteAttendsメソッドを呼び出している
			//deleteAttendsメソッドの引数にattendsを渡す
			attendsDao.deleteAttends(attends);

			//0528前田により変更、フォワードではevent_idをEventDetailServletに送れない
			//event_idは保持したまま、response.sendRedirectを使い、EventDetailServletへ飛ぶ
			response.sendRedirect("EventDetailServlet?id=" + event_id);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
