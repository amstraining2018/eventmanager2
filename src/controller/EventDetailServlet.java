package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoFactory;
import dao.EventDao;
import dao.UserDao;
import domain.Event;
import domain.User;

/**
 * Servlet implementation class EventDetailServlet
 */
@WebServlet("/EventDetailServlet")
public class EventDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/****************************TodaysEventからイベントidを取得********************************************************/
		int event_id = Integer.parseInt(request.getParameter("id"));

		//ユーザーID（String 取得）
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		String login_id = (String) session.getAttribute("LOGIN_ID");

		try {
			EventDao eventDao = DaoFactory.createEventDao();
			UserDao userDao = DaoFactory.createUserDao();

			//イベント詳細表示
			List<Event> eventList = eventDao.findDetail(event_id);
			List<User> attendsIdlist = userDao.findByAttendsId(event_id);
			int user_id = userDao.findIdByLoginId(login_id);
			//参加取り消しボタンを表示するかどうか
			boolean sankasinaiButton = eventDao.sankasinaiButton(user_id, event_id);
			//編集、削除（管理者、登録者、）を表示するかどうか
			//編集削除ボタン表示するかしないか（管理者）

			int REGISTERED_BY = eventDao.getREGISTERED_BY(event_id);
			int userType_id = (int) session.getAttribute("userType");

			if (user_id == REGISTERED_BY || userType_id == 2) {

				request.setAttribute("EditAndDeleteButton", true);
			} else {

			}

			request.setAttribute("eventList", eventList);
			request.setAttribute("AttendsIdList", attendsIdlist);
			request.setAttribute("event_id", event_id);
			request.setAttribute("user_id", user_id);
			request.setAttribute("sankasinaiButton", sankasinaiButton);

			//

			request.getRequestDispatcher("WEB-INF/view/eventDetail.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

}