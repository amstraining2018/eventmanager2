package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDao;
import dao.DaoFactory;
import domain.Admin;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//login画面に遷移
		try {
			request.getRequestDispatcher("WEB-INF/view/login.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String loginId = request.getParameter("LOGIN_ID");
		String loginPass = request.getParameter("LOGIN_PASS");
		//ログイン画面で入力したid,passを上記で取得

		try {
			AdminDao adminDao = DaoFactory.createAdminDao();
			Admin admin = adminDao.findByLoginIdAndLoginPass(loginId, loginPass);
			//adminDao.findByLoginIdAndLoginPass(loginId, loginPass)でDBからid,passが一致する行を取得
			if (admin != null) {
				//adminに入っていた場合（idとpassがDBから見つかった場合）
				request.getSession().setAttribute("userType", admin.getUserType());
				request.getSession().setAttribute("name", admin.getLoginName());
				request.getSession().setAttribute("LOGIN_ID", admin.getLoginId());
				request.getSession().setAttribute("userId", admin.getId());
				request.getSession().setAttribute("LOGIN_PASS", admin.getLoginPass());
				//上記4行で一般ユーザーか管理ユーザーか、ユーザー名、ユーザーID、テーブル上のIDをセッション保持
				response.sendRedirect("TodaysEventServlet");
				//上記で今日の一覧へページ遷移
			} else {
				//idとpassがDBから見つからなかった場合
				request.setAttribute("error", true);
				//エラーメッセージを出す処理
				request.getRequestDispatcher("WEB-INF/view/login.jsp").forward(request, response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
