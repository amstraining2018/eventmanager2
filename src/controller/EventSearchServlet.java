package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AttendsDao;
import dao.DaoFactory;
import dao.EventDao;
import dao.RoomDao;
import dao.UserDao;
import domain.Attends;
import domain.Event;
import domain.Group_kotaki;
import domain.Room;
import domain.User;

/**
 * Servlet implementation class EventSearchServlet
 */
@WebServlet("/EventSearchServlet")
public class EventSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventSearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int group_id = 0;
		int room_id = 0;
		if (request.getParameter("group_id") == null) {
			group_id = 0;
		} else {
			group_id = Integer.parseInt(request.getParameter("group_id"));
		}
		if (request.getParameter("room_id") == null) {
			room_id = 0;
		} else {
			room_id = Integer.parseInt(request.getParameter("room_id"));
		}

		//eventDaoで検索
		try {

			//***表示するページ情報のためのリクエスト取得して送る***
			String pageNumAsString = request.getParameter("nowPageNumber");
			//String型のpageNumAsStringの中に
			int pageNumAsInt = 1;
			//int型のpageNumAsIntを定義し、その中に1をいれている
			if (pageNumAsString == null) {
				request.setAttribute("nowPageNumber", pageNumAsInt);
				//pageNumAsStringの値がnullの場合は
				//pageNumAsIntに1をいれていく。
			} else {
				pageNumAsInt = Integer.parseInt(pageNumAsString);
				request.setAttribute("nowPageNumber", pageNumAsInt);
				//上の条件（pageNumAsStringがnullだった場合）ではない場合は
				//pageNumAsIntに1をいれる。
			}

			//****************************************************************
			HttpServletRequest req = (HttpServletRequest) request;
			//HttpServletRequestの実行する情報を"req"に代入する。
			HttpSession session = req.getSession();
			//HttpSessionの実行する情報を"session"に代入する。
			String login_id = (String) session.getAttribute("LOGIN_ID");
			//ログインしたIDをString型の"login_id"にString型にキャストした"session"を用いて代入する。

			//セッションに保存されておるTYPE_IDを取得
			int type_id = (Integer) session.getAttribute("userType");
			//TYPE_IDをsetAttributeしてjspに送る
			request.setAttribute("type_id", type_id);

			//***表示するデータベースの情報を取得して送る***
			//定義
			EventDao eventDao = DaoFactory.createEventDao();
			//DaoFactoryのcreateEventDaoの実行する情報(new EventDaoImpl(getDataSource())を呼びだすメソッド)を
			//"eventDao"に代入する。
			AttendsDao attendsDao = DaoFactory.createAttendsDao();
			//DaoFactoryのcreateAttendsDaoの実行する情報(new AdminDaoImpl(getDataSource())を呼びだすメソッド)を
			//"attendsDao"に代入する。
			UserDao userDao = DaoFactory.createUserDao();
			//DaoFactoryのcreateUserDaoの実行する情報(new UserDaoImpl(getDataSource())を呼びだすメソッド)を
			//"userDao"に代入する。
			RoomDao roomDao = DaoFactory.createRoomDao();

			//*************************************************//
			List<Event> eventList = eventDao.searchForEventManage(pageNumAsInt, group_id, room_id);
			//eventDaoのfindForEventManageの実行結果を"eventList"に格納していく。
			List<User> LoginInfo = userDao.getLoginInfo(login_id);
			//"userDao.getLoginInfo"で実行した"login_id"を"LoginInfo"に代入する。
			int id = LoginInfo.get(0).getId();
			//"LoginInfo"の中の"login_id"をint型に変換する。
			//(元はString型になっているため。)
			/////////////////////////////////////////////////////////////////////////////////////長島追加0612
			//部署名のテーブルを全部取得
			List<Group_kotaki> groups = eventDao.findAllGroupName();

			//Room情報を取得
			List<Room> rooms = roomDao.getRooms_nameAndRooms_id();
			Group_kotaki dk = new Group_kotaki();
			dk.setGroup_name("所属を選択してください");
			groups.add(0, dk);
			Room rm = new Room();
			rm.setName("会議室を選択してください");
			rooms.add(0, rm);

			request.setAttribute("groups", groups);
			request.setAttribute("rooms", rooms);

			request.setAttribute("groupId", group_id);
			request.setAttribute("roomId", room_id);

			/////////////////////////////////////////////////////////////////////////////////////長島追加0612
			List<Attends> attendsList = attendsDao.getAttends(id);
			//"attendsDao.getAttends"で実行した"id"を"attendsList"に代入する。
			request.setAttribute("attendsList", attendsList);
			//attendsListにある値を"attendsList"という変数名で送れるようにする。
			request.setAttribute("eventList", eventList);
			//eventListにある値を"eventList"という変数名で送れるようにする。
			//変更

			//****************************************************************

			//***全ページ数を送る***
			int allPageNumber = eventDao.getEventManageAllPageNumber(group_id, room_id);
			//eventDao.getEventManageAllPageNumberで実行したメソッドをint型で"allPageNumber"に代入する。
			request.setAttribute("allPageNumber", allPageNumber);
			//allPageNumberにある値を"allPageNumber"という変数名で送れるようにする。
			//************************

			request.getRequestDispatcher("WEB-INF/view/eventSearchManage.jsp").forward(request, response);

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int group_id = 0;
		int room_id = 0;
		if (request.getParameter("group_id") == null) {
			group_id = 0;
		} else {
			group_id = Integer.parseInt(request.getParameter("group_id"));
		}
		if (request.getParameter("room_id") == null) {
			room_id = 0;
		} else {
			room_id = Integer.parseInt(request.getParameter("room_id"));
		}

		//eventDaoで検索
		try {

			//***表示するページ情報のためのリクエスト取得して送る***
			String pageNumAsString = request.getParameter("nowPageNumber");
			//String型のpageNumAsStringの中に
			int pageNumAsInt = 1;
			//int型のpageNumAsIntを定義し、その中に1をいれている
			if (pageNumAsString == null) {
				request.setAttribute("nowPageNumber", pageNumAsInt);
				//pageNumAsStringの値がnullの場合は
				//pageNumAsIntに1をいれていく。
			} else {
				pageNumAsInt = Integer.parseInt(pageNumAsString);
				request.setAttribute("nowPageNumber", pageNumAsInt);
				//上の条件（pageNumAsStringがnullだった場合）ではない場合は
				//pageNumAsIntに1をいれる。
			}

			//****************************************************************
			HttpServletRequest req = (HttpServletRequest) request;
			//HttpServletRequestの実行する情報を"req"に代入する。
			HttpSession session = req.getSession();
			//HttpSessionの実行する情報を"session"に代入する。
			String login_id = (String) session.getAttribute("LOGIN_ID");
			//ログインしたIDをString型の"login_id"にString型にキャストした"session"を用いて代入する。

			//セッションに保存されておるTYPE_IDを取得
			int type_id = (Integer) session.getAttribute("userType");
			//TYPE_IDをsetAttributeしてjspに送る
			request.setAttribute("type_id", type_id);

			//***表示するデータベースの情報を取得して送る***
			//定義
			EventDao eventDao = DaoFactory.createEventDao();
			//DaoFactoryのcreateEventDaoの実行する情報(new EventDaoImpl(getDataSource())を呼びだすメソッド)を
			//"eventDao"に代入する。
			AttendsDao attendsDao = DaoFactory.createAttendsDao();
			//DaoFactoryのcreateAttendsDaoの実行する情報(new AdminDaoImpl(getDataSource())を呼びだすメソッド)を
			//"attendsDao"に代入する。
			UserDao userDao = DaoFactory.createUserDao();
			//DaoFactoryのcreateUserDaoの実行する情報(new UserDaoImpl(getDataSource())を呼びだすメソッド)を
			//"userDao"に代入する。
			RoomDao roomDao = DaoFactory.createRoomDao();

			//*************************************************//
			List<Event> eventList = eventDao.searchForEventManage(pageNumAsInt, group_id, room_id);
			//eventDaoのfindForEventManageの実行結果を"eventList"に格納していく。
			List<User> LoginInfo = userDao.getLoginInfo(login_id);
			//"userDao.getLoginInfo"で実行した"login_id"を"LoginInfo"に代入する。
			int id = LoginInfo.get(0).getId();
			//"LoginInfo"の中の"login_id"をint型に変換する。
			//(元はString型になっているため。)
			/////////////////////////////////////////////////////////////////////////////////////長島追加0612
			//部署名のテーブルを全部取得
			List<Group_kotaki> groups = eventDao.findAllGroupName();

			//Room情報を取得
			List<Room> rooms = roomDao.getRooms_nameAndRooms_id();
			Group_kotaki dk = new Group_kotaki();
			dk.setGroup_name("所属を選択してください");
			groups.add(0, dk);
			Room rm = new Room();
			rm.setName("会議室を選択してください");
			rooms.add(0, rm);

			request.setAttribute("groups", groups);
			request.setAttribute("rooms", rooms);

			request.setAttribute("groupId", group_id);
			request.setAttribute("roomId", room_id);

			/////////////////////////////////////////////////////////////////////////////////////長島追加0612
			List<Attends> attendsList = attendsDao.getAttends(id);
			//"attendsDao.getAttends"で実行した"id"を"attendsList"に代入する。
			request.setAttribute("attendsList", attendsList);
			//attendsListにある値を"attendsList"という変数名で送れるようにする。
			request.setAttribute("eventList", eventList);
			//eventListにある値を"eventList"という変数名で送れるようにする。
			//変更

			//****************************************************************

			//***全ページ数を送る***
			int allPageNumber = eventDao.getEventManageAllPageNumber(group_id, room_id);
			//eventDao.getEventManageAllPageNumberで実行したメソッドをint型で"allPageNumber"に代入する。
			request.setAttribute("allPageNumber", allPageNumber);
			//allPageNumberにある値を"allPageNumber"という変数名で送れるようにする。
			//************************

			request.getRequestDispatcher("WEB-INF/view/eventSearchManage.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
}
