package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		int a = Integer.parseInt(request.getParameter("delete"));
		//userDetail.jspからrequest.getParameterを使い、deleteを取得
		//Integer.parseIntを使い、int型の変数aに格納

		try {
			UserDao UserDao = DaoFactory.createUserDao();

			UserDao.delete(a);
			//UserDaoにあるdeleteメソッドを呼び出している
			//deleteメソッドの引数にaを渡す

		} catch (Exception e) {
			throw new ServletException(e);
		}
		request.getRequestDispatcher("WEB-INF/view/userDeleteComp.jsp").forward(request, response);
		//userDeleteComp.jspへ遷移
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//		int a = Integer.parseInt(request.getParameter("delete"));
		//		//userDetail.jspからrequest.getParameterを使い、deleteを取得
		//		//Integer.parseIntを使い、int型の変数aに格納
		//
		//		try {
		//			UserDao UserDao = DaoFactory.createUserDao();
		//
		//			UserDao.delete(new Integer(a));
		//			//
		//
		//		} catch (Exception e) {
		//			throw new ServletException(e);
		//		}
		//		request.getRequestDispatcher("userDeleteComp.jsp").forward(request, response);
		//		//userDeleteComp.jspに遷移する
	}

}
