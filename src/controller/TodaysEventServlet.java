package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AttendsDao;
import dao.DaoFactory;
import dao.EventDao;
import dao.UserDao;
import domain.Attends;
import domain.Event;
import domain.User;

/**
 * Servlet implementation class TodaysEventServle
 */
@WebServlet("/TodaysEventServlet")
public class TodaysEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			EventDao eventDao = DaoFactory.createEventDao();
			AttendsDao attendsDao = DaoFactory.createAttendsDao();
			UserDao userDao = DaoFactory.createUserDao();

			/********************************「参加」表示用******************************************************/
			//セッションで保管されている情報から参加者のIDを取得
			HttpServletRequest req = (HttpServletRequest) request;
			HttpSession session = req.getSession();
			String login_id = (String) session.getAttribute("LOGIN_ID");
			List<User> LoginInfo = userDao.getLoginInfo(login_id);
			int id = LoginInfo.get(0).getId();
			List<Attends> attendsList = attendsDao.getAttends(id);

			/********************************ページネーション******************************************************/
			/********************************5項目ごと*************************************************************/
			String CurrentPageNumberString = (request.getParameter("CurrentPageNumber"));
			int CurrentPageNumberInt = 1;

			if (CurrentPageNumberString == null) {
				request.setAttribute("CurrentPageNumber", CurrentPageNumberInt);
			} else {
				CurrentPageNumberInt = Integer.parseInt(CurrentPageNumberString);
				request.setAttribute("CurrentPageNumber", CurrentPageNumberInt);
			}
			/********************************イベント情報表示用******************************************************/
			List<Event> eventList = eventDao.findTodaysEvent(CurrentPageNumberInt);
			request.setAttribute("eventList", eventList);
			request.setAttribute("attendsList", attendsList);

			/********************************総ページ数******************************************************/
			int pages = eventDao.getPageNumbers();
			request.setAttribute("pages", pages);

			request.getRequestDispatcher("WEB-INF/view/todaysEvent.jsp").forward(request, response);

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
