package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoFactory;
import dao.EventDao;
import dao.RoomDao;
import dao.UserDao;
import domain.Event;

/**
 * Servlet implementation class EvenyRegistServlet
 */
@WebServlet("/EvenyRegistServlet")
public class EvenyRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EvenyRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			UserDao userDao = DaoFactory.createUserDao();
			RoomDao roomDao = DaoFactory.createRoomDao();

			List<String> typesList = userDao.findtypesAll();
			List<String> typesidList = userDao.findtypesidAll();
			List<String> roomIdList = roomDao.getRooms_id();
			List<String> roomNameList = roomDao.getRooms_name();

			request.setAttribute("typesList", typesList); //userDaoImplからnameを取得
			request.setAttribute("typesidList", typesidList); //userDaoImplからidを取得
			request.setAttribute("roomIdList", roomIdList);
			request.setAttribute("roomNameList", roomNameList);
		} catch (Exception e) {
			throw new ServletException(e);
		}
		request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);//eventRegist.jspへ
	}




	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String namet = request.getParameter("namet").trim();//前の画面で受け取ったものを取得
		String namer = request.getParameter("namer");
///////////////////////ここぞ////////////////ここ//////////////////////ここやで/////////////////////////////////////////
		String room_name = request.getParameter("room_name").trim();
		String group_name = request.getParameter("group_name").trim();

		Event event = new Event();//インスタンス
		event.setTitle(namet);//eventのTitleにset
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////日付のフォーマット//////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

		boolean startAlertFlag = false;
		boolean endAlertFlag = false;
		boolean endCompareStartAlertFlag = false;
		boolean chkDateS = false;
		boolean chkDateE = false;
		String names = (request.getParameter("names"));
		String namef = (request.getParameter("namef"));


		sdf.setLenient(false);
		try {
			sdf.parse(names);
			request.setAttribute("chkStartFlag", false);
			chkDateS = false;
		} catch (Exception e) {
			request.setAttribute("chkStartFlag", true);
			chkDateS = true;
		}
		try {
			sdf.parse(namef);
			request.setAttribute("chkEndFlag", false);
			chkDateE = false;
		} catch (Exception e) {
			request.setAttribute("chkEndFlag", true);
			chkDateE = true;
		}

		Date nowDate = new Date();
		Date formatednow = null;
		try {
			formatednow = sdf.parse(sdf.format(nowDate));
		} catch (ParseException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}

		Date formatedStart = null;
		try {
			formatedStart = sdf.parse(names);
			startAlertFlag = formatedStart.before(formatednow);
		} catch (ParseException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}


		if (startAlertFlag) {
			request.setAttribute("startPastTimeAlertFlag", true);
		} else {
			request.setAttribute("startPastTimeAlertFlag", false);
		}
		event.setStart(formatedStart);//eventのStartにset

		if (!namef.equals("")) {
			Date formatedEnd = null;
			try {
				formatedEnd = sdf.parse(namef);
				endAlertFlag = formatedEnd.before(formatednow);
			} catch (ParseException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			if (endAlertFlag) {
				request.setAttribute("endPastTimeAlertFlag", true);
			} else {
				request.setAttribute("endPastTimeAlertFlag", false);
			}
			event.setEnd(formatedEnd);
			try {
				formatedEnd = sdf.parse(namef);
			} catch (ParseException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			try {
			endCompareStartAlertFlag = formatedEnd.before(formatedEnd);
			}catch(NullPointerException e) {

			}
			if (endCompareStartAlertFlag) {
				request.setAttribute("endCompareStartTimeAlertFlag", true);
			} else {
				request.setAttribute("endCompareStartTimeAlertFlag", false);
			}
		}

		event.setDetail(namer);
		event.setGroup_name(group_name);//eventのGroup_nameにset

		try {
			EventDao eventDao = DaoFactory.createEventDao();
			RoomDao roomDao = DaoFactory.createRoomDao();

			int groupId = eventDao.findById(group_name);//EventDaoImplのfindByIdからgroupIdに入れる
			int room_id = roomDao.findByroom_Id(room_name);

			event.setRooms_id(room_id);
			event.setGroup_id(groupId);//eventのGroup_idにset

			HttpSession session = request.getSession();
			Integer userId = (Integer) session.getAttribute("userId");//セッションでユーザーIDを取得

			if (startAlertFlag || endAlertFlag || endCompareStartAlertFlag||chkDateS ||chkDateE) {
				try {
					UserDao userDao = DaoFactory.createUserDao();

					List<String> typesList = userDao.findtypesAll();
					List<String> typesidList = userDao.findtypesidAll();
					List<String> roomIdList = roomDao.getRooms_id();
					List<String> roomNameList = roomDao.getRooms_name();


					request.setAttribute("typesList", typesList); //userDaoImplからnameを取得
					request.setAttribute("typesidList", typesidList); //userDaoImplからidを取得
					request.setAttribute("roomIdList", roomIdList);
					request.setAttribute("roomNameList", roomNameList);

				} catch (Exception e) {
					throw new ServletException(e);
				}
				request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);//eventRegist.jspへ
			} else {
				eventDao.insert(event, userId);//eventDaoImplのinsert
				request.getRequestDispatcher("WEB-INF/view/eventRegistComp.jsp").forward(request, response);//eventRegistComp.jspへ
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

}