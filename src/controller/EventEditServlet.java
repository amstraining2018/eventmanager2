package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.EventDao;
import dao.RoomDao;
import domain.Event;
import domain.Group_kotaki;
import domain.Room;

/**
 * Servlet implementation class EventEditServlet
 */
@WebServlet("/EventEditServlet")
public class EventEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int event_id = Integer.parseInt(request.getParameter("id"));

		try {
			//***表示するデータベースの情報を取得して送る***//
			RoomDao roomDao = DaoFactory.createRoomDao();
			EventDao eventDao = DaoFactory.createEventDao();


			//イベントIDが参照したイベントのデータを取得
			Event event = eventDao.findByIdForEventEdit(event_id);

			//部署名のテーブルを全部取得
			List<Group_kotaki> groups = eventDao.findAllGroupName();

			//Room情報を取得
			List<Room> rooms = roomDao.getRooms_nameAndRooms_id();


			//セッション保持
			//****************************************************************



			request.getSession().setAttribute("event", event);
			request.getSession().setAttribute("event_id", event_id);
			request.getSession().setAttribute("groups", groups);
			request.getSession().setAttribute("rooms", rooms);
			request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);




		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
