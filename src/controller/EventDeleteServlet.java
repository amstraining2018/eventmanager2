package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.EventDao;

/**
 * Servlet implementation class EventDeleteServlet
 */
@WebServlet("/EventDeleteServlet")
public class EventDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int event_id = Integer.parseInt(request.getParameter("id"));
		//eventDetail.jspからrequest.getParameterを使い、idを取得
		//Integer.parseIntを使い、int型のevent_idに格納

		try {
			EventDao EventDao = DaoFactory.createEventDao();

			EventDao.delete(event_id);
			//EventDaoImplにあるdeleteメソッドを呼び出している
			//deleteメソッドの引数にevent_idを渡す

			request.getRequestDispatcher("WEB-INF/view/eventDeleteComp.jsp").forward(request, response);
			//eventDeleteComp.jspへ遷移

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int event_id = Integer.parseInt(request.getParameter("id"));
		//eventDetail.jspからrequest.getParameterを使い、idを取得
		//Integer.parseIntを使い、int型のevent_idに格納

		try {
			EventDao EventDao = DaoFactory.createEventDao();

			EventDao.delete(event_id);
			//EventDaoImplにあるdeleteメソッドを呼び出している
			//deleteメソッドの引数にevent_idを渡す

			request.getRequestDispatcher("WEB-INF/view/eventDeleteComp.jsp").forward(request, response);
			//eventDeleteComp.jspへ遷移

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

}
