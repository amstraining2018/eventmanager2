package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendsDao;
import dao.DaoFactory;
import domain.Attends;

/**
 * Servlet implementation class EventSubmitServlet
 */
@WebServlet("/EventSubmitServlet")
public class EventSubmitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventSubmitServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	//参加のボタンを押した際に呼び出すサーブレット
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//attendsテーブルの"event_id"をint型のevent_idに代入する。
		int event_id = Integer.parseInt(request.getParameter("event_id"));

		//attendsテーブルの"user_id"をint型のuser_idに代入する。
		int user_id = Integer.parseInt(request.getParameter("user_id"));

		//domainのAttendsをattendsに代入する。
		Attends attends = new Attends();

		//attendsの"Event_id"の中にevent_idの値を代入する。
		attends.setEvent_id(event_id);

		//attendsの"User_id"の中にuser_idの値を代入する。
		attends.setUser_id(user_id);

		//AttendsDaoImplを使用できるようにDaoFactory.createAttendsDaoから
		//attendsDaoにメソッドを使用できるようにする。
		//attendsDaoに先ほどのattends(event_idとuser_id)を代入する。
		//response.sendRedirectでEventDetailServletの
		//「int event_id = Integer.parseInt(request.getParameter("id"));」
		//のidにevent_idを持たせる

		try {
			AttendsDao attendsDao = DaoFactory.createAttendsDao();
			attendsDao.insertAttends(attends);
			//0528前田により変更、フォワードではevent_idをEventDetailServletに送れない
			response.sendRedirect("EventDetailServlet?id=" + event_id);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
