package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;
import domain.UserName;

/**
 * Servlet implementation class UserRegistServlet
 */
@WebServlet("/userRegist")
public class UserRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserDao userDao = DaoFactory.createUserDao();

		try {

			//ユーザー登録ﾎﾞﾀﾝ表示用
			boolean flg = userDao.registUser();
			if(flg==true) {

			int pageNumAsInt = 1;
			List<User> userList = userDao.findForUserManage(pageNumAsInt);
			int allPageNumber = userDao.getUserManageAllPageNumber();
			request.setAttribute("flg", "0");
			request.setAttribute("userList", userList);
			request.setAttribute("allPageNumber", allPageNumber);
			request.setAttribute("login_id", "");
			request.setAttribute("login_pass", "");
			//loginidとpassの初期値をいれる
			request.setAttribute("allPageNumber", allPageNumber);



			request.getRequestDispatcher("WEB-INF/view/userManage.jsp").forward(request, response);
			return;
			}
		} catch (Exception e) {

		}


		//名前
		try {

			//EmployeeテーブルにあってuserテーブルにないEmployee_idの情報を取得
			List<UserName> UserInfoList = userDao.getUserInfomation();
			int err1 = 0;
			int err2 = 0;
			request.setAttribute("err1", err1);
			request.setAttribute("err2", err2);
			request.setAttribute("UserInfoList", UserInfoList);
			request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserDao userDao = DaoFactory.createUserDao();

		String employee_id_List = request.getParameter("employee_id_List");
		String[] list = employee_id_List.split(",", 0);
		String employee_id = list[0];
		String login_id = request.getParameter("input_login_id");
		String login_pass = request.getParameter("input_logiin_pass");

		//「選択してください」を選択した場合
		if (employee_id.equals("選択してください")) {
			try {
				int err1 = -1;
				int err2 = 0;
				List<UserName> UserInfoList = userDao.getUserInfomation();
				request.setAttribute("UserInfoList", UserInfoList);
				request.setAttribute("err1", err1);
				request.setAttribute("err2", err2);
				request.setAttribute("login_id", login_id);
				request.setAttribute("login_pass", login_pass);
				request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);
				return;
			} catch (Exception e) {
				throw new ServletException(e);
			}

		}
		//入力されたログインIDがすでにぞんざいする場合
		try {
			//usersテーブルからlogin_id全てを取得するメソッドをuserDaoにつくる
			List<String> usersList = userDao.listLogin_id();
			//login_idのリストと入力されたlogin_idが一致したときの処理
			if (usersList.contains(login_id)) {
				int err1 = 0;
				int err2 = -1;
				List<UserName> UserInfoList = userDao.getUserInfomation();
				request.setAttribute("UserInfoList", UserInfoList);
				request.setAttribute("err1", err1);
				request.setAttribute("err2", err2);
				request.setAttribute("login_id", login_id);
				request.setAttribute("login_pass", login_pass);
				request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);
				return;
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}

		//登録処理

		User user = new User();
		login_pass = BCrypt.hashpw(login_pass, BCrypt.gensalt());
		user.setLogin_id(login_id);
		user.setLogin_pass(login_pass);
		user.setEmployee_id(employee_id);

		try {

			//userDaoImplのinsert
			userDao.insert(user);
			request.getRequestDispatcher("WEB-INF/view/userRegistComp.jsp").forward(request, response);
		}

		catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
