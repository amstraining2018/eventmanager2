package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;

/**
 * Servlet implementation class UserPasswordServlet
 */
@WebServlet("/UserPasswordServlet")
public class UserPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserPasswordServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		Integer id = (Integer) session.getAttribute("userId");
		try {

			//****************************************************************

			//***表示するデータベースの情報を取得して送る***

			UserDao userDao = DaoFactory.createUserDao();
			;
			//入力欄に入れるやつ
			List<String> typesList = userDao.findtypesAll(); //UserDaoImplからnameを取得
			List<String> typesidList = userDao.findtypesidAll(); //UserDaoImplからidを取得
			request.setAttribute("typesList", typesList);
			request.setAttribute("typesi"
					+ "dList", typesidList);
			User user = new User();
			user = userDao.findById_jyo(id);
			request.setAttribute("user", user);
			//仮
			request.setAttribute("errmsg", "");

			request.getRequestDispatcher("WEB-INF/view/userPassword.jsp").forward(request, response);

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDao userDao = DaoFactory.createUserDao();

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		String login_id = (String) session.getAttribute("LOGIN_ID");
		String login_pass = (String) session.getAttribute("LOGIN_PASS");
		int id = (int) session.getAttribute("userId");
		String newLogin_pass = request.getParameter("newLogin_pass");
		System.out.println(login_pass);

		//Stringの中に番号を抽出する
		//String型をint型に変換している？
		//int ret = 0;

		User user = new User();

		//user.setLogin_id(login_id);
		//String hashed = BCrypt.hashpw(login_pass, BCrypt.gensalt());
		//	user.setLogin_pass(login_pass);

		try {
			if (!BCrypt.checkpw(request.getParameter("oldLogin_pass"), login_pass)) {

				request.setAttribute("errmsg", "旧パスワードが違います");
				request.getRequestDispatcher("WEB-INF/view/userPassword.jsp").forward(request, response);
			} else if (request.getParameter("oldLogin_pass").equals(request.getParameter("newLogin_pass"))) {

				request.setAttribute("errmsg2", "旧と新パスワードが同じです");
				request.getRequestDispatcher("WEB-INF/view/userPassword.jsp").forward(request, response);
			} else if (!request.getParameter("newLogin_pass").equals(request.getParameter("saiLogin_pass"))) {

				request.setAttribute("errmsg3", "再入力と新パスワードが異なります");
				request.getRequestDispatcher("WEB-INF/view/userPassword.jsp").forward(request, response);
			}
			String hashed = BCrypt.hashpw(newLogin_pass, BCrypt.gensalt());
			user.setLogin_pass(hashed);
			System.out.println(hashed);
			user.setLogin_id(login_id);
			System.out.println(login_id);
			System.out.println(login_pass);

			user.setId(id);

			userDao.update_pass(user);
			request.setAttribute("user", user);
			System.out.println(userDao);
			request.getRequestDispatcher("WEB-INF/view/userPasswordComp.jsp").forward(request, response);

		} catch (Exception e) {

			throw new ServletException(e);
		}
	}
}
