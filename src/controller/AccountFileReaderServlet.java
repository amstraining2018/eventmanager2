package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;
import util.AccountFileReadMethod;
import util.PropertiesGetter;

/**
 * Servlet implementation class AccountFileReaderServlet
 */
@WebServlet("/AccountFileReaderServlet")
public class AccountFileReaderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String ADDRESS = "/WEB-INF/classes/fileRead.properties";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountFileReaderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext context = this.getServletContext();
		String path = context.getRealPath(ADDRESS);
		PropertiesGetter p = new PropertiesGetter();
		//		p.getProperties(path);
		AccountFileReadMethod a = new AccountFileReadMethod();
		String fileType="account";
		List<User> userList = a.readCsvFile(p.getProperties(fileType, path));
		String err=a.err;

		if(err==null) {
			UserDao UserDao = DaoFactory.createUserDao();
			for (User user : userList) {
				try {
					UserDao.insertUser(user);
					} catch (Exception e) {
						//東デーモン イデデ大王によるINSERTIGNOREの再現

				}

			}

		}else {
			request.setAttribute("err", err);
		}

		request.getRequestDispatcher("UserManageServlet").forward(request, response);



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
