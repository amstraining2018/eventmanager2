﻿package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoFactory;
import dao.EventDao;

/**
 * Servlet implementation class EventUpdateServlet
 */
@WebServlet("/EventUpdateServlet")
public class EventUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//eventDaoimplでSQL文を処理しやすいようにtitleからdetailまで配列で保持
		//[0]でMySQLのカラム名、[1]で編集したデータ、[2]で型を保持
		String event_id = request.getParameter("event_id");
		String[] title = { "title", request.getParameter("title"), "string" };
		String[] start = { "start", request.getParameter("start"), "string" };
		String[] end = { "end", request.getParameter("end"), "string" };
		String[] room_id = { "room_id", request.getParameter("room_id"), "int" };
		String[] group_id = { "group_id", request.getParameter("group_name"), "int" };
		String[] detail = { "detail", request.getParameter("detail"), "string" };

		String[][] update = { title, start, end, room_id, group_id, detail };
		//二重配列で全部のデータを保持

		//***日付に関する処理****
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

		//ハイフンをスラッシュに
		start[1] = start[1].replace("-", "/");
		end[1] = end[1].replace("-", "/");

		//***日付に関する処理終わり****

		try {

			//**************開始時刻や終了時刻に過去の値が入力されている場合の処理*******************
			boolean startAlertFlag = false;
			boolean endAlertFlag = false;
			boolean endCompareStartAlertFlag = false;
			boolean chkDateS = false;
			boolean chkDateE = false;
			//フラグを設定
			sdf.setLenient(false);
			try {
				sdf.parse(start[1]);
				request.setAttribute("chkStartFlag", false);
				chkDateS = false;
			} catch (Exception e) {
				request.setAttribute("chkStartFlag", true);
				chkDateS = true;
			}
			try {
				sdf.parse(end[1]);
				request.setAttribute("chkEndFlag", false);
				chkDateE = false;
			} catch (Exception e) {
				request.setAttribute("chkEndFlag", true);
				chkDateE = true;
			}
			Date nowDate = new Date();
			Date formatednow = sdf.parse(sdf.format(nowDate));
			//過去の時間でないか比較するため現在の時間を取得
			Date formatedStart = null;
			try {
				formatedStart = sdf.parse(start[1]);
				startAlertFlag = formatedStart.before(formatednow);
			} catch (ParseException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}

			//開始日時が過去の場合falseがtrueになる、if文の中を実行jspでエラー表示になる
			if (startAlertFlag) {
				request.setAttribute("startPastTimeAlertFlag", true);
			} else {
				request.setAttribute("startPastTimeAlertFlag", false);
			}

			if (!end[1].equals("")) {
				//終了日時が空欄じゃない場合日時をフォーマット化
				Date formatedEnd = null;
				try {
					formatedEnd = sdf.parse(end[1]);
					endAlertFlag = formatedEnd.before(formatednow);
				} catch (ParseException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
				//終了日時が過去の場合falseがtrueになる、if文の中を実行jspでエラー表示になる
				if (endAlertFlag) {
					request.setAttribute("endPastTimeAlertFlag", true);
				} else {
					request.setAttribute("endPastTimeAlertFlag", false);
				}

				try {
					endCompareStartAlertFlag = formatedEnd.before(formatedStart);
				} catch (NullPointerException e) {

				}
				if (endCompareStartAlertFlag) {
					//開始日時が終了日時より前の場合falseがtrueに、エラー表示になる
					request.setAttribute("endCompareStartTimeAlertFlag", true);
				} else {
					request.setAttribute("endCompareStartTimeAlertFlag", false);
				}
			}

			if (startAlertFlag || endAlertFlag || endCompareStartAlertFlag || chkDateS || chkDateE) {
				//上記のどれかがtrueの場合、編集画面に戻る
				request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);
				return;
			} else {
				HttpSession session = request.getSession();
				//保持していたセッションを破棄
				session.removeAttribute("event");
				session.removeAttribute("groups");
				session.removeAttribute("event_id");
			}
			//***********************************************************************************************

			EventDao eventDao = DaoFactory.createEventDao();

			eventDao.updateForEventEdit(event_id, update);

			//イベントIDを送る
			request.setAttribute("event_id", event_id);

			request.getRequestDispatcher("WEB-INF/view/eventEditComp.jsp").forward(request, response);

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

}
