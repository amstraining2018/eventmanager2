package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.EventDao;
import domain.Room;
import util.FileReadMethod;
import util.PropertiesGetter;

/**
 * Servlet implementation class FileReaderServlet
 */
@WebServlet("/FileReaderServlet")
public class FileReaderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String ADDRESS = "/WEB-INF/classes/fileRead.properties";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FileReaderServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext context = this.getServletContext();
		String path = context.getRealPath(ADDRESS);
		PropertiesGetter p = new PropertiesGetter();
		//		p.getProperties(path);
		FileReadMethod frm = new FileReadMethod();
		String fileType = "room";
		List<Room> RoomList = frm.readCsvFile(p.getProperties(fileType, path));
		String errmsg = frm.errmgs;
		System.out.println(errmsg);
		if (errmsg == null) {
			EventDao EventDao = DaoFactory.createEventDao();
			for (Room room : RoomList) {
				try {
					EventDao.insertRoom(room);
				} catch (Exception e) {
					//東デーモン イデデ大王によるINSERTIGNOREの再現

				}

			}
		} else {
			request.setAttribute("errmsg", errmsg);
			//エラーメッセージを送る
		}

		request.getRequestDispatcher("EventServlet").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
