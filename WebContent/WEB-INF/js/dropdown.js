// dropdown menu の項目 (class "js-group-name" が付与されたタグ) がクリックされたら...
$( '.js-group-name' ).on( 'click', function()
{
  /* クリックされた要素 (class "js-group-name" が付与されたタグ) 内に書かれたテキストを
   * inputタグ (class "js-group-input" が付与されたタグ) に入力する
   */
  $( this ).parents( '.js-choose-group' ).find( '.js-group-input' ).val( $( this ).text() );
});
