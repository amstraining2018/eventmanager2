<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<%
	int event_id = (Integer) request.getAttribute("event_id");
%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html>




<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css" rel="stylesheet" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<br>
<br>
<br>
<title>イベント管理</title>








<script>
	$(document).ready(
			function() {

				$('#submit').click(
						function() {
							if ($('input[name="title"]').val() == ""
									| $('input[name="start"]').val() == "") {
								swal("必須項目を入力してください");
							} else {
								$("#submit").attr("type", "submit");
								$('form').submit();
							}
							;
						});
			});
</script>



</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="2" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>


			<h2>イベント編集</h2>

			<!-- ▼ 入力フォーム -->
			<form action="EventUpdateServlet" method="post" id="form">

				<input type="hidden" name="event" value="<c:out value="${event}"/>">
				<input type="hidden" name="groups"
					value="<c:out value="${groups}"/>"> <input type="hidden"
					name="event_id" value="<c:out value="${event_id}"/>"> <label>タイトル
					(必須)</label>
<!--  -->
				<p>
					<input type="text" required name="title" class="form-control"
						maxlength="50" value="<c:out value="${event.title }" />" />

				</p>
				<label>開始日時 (必須)</label>

				<!-- ▼ アラート表示ここから -->
				<c:if test="${startPastTimeAlertFlag}">
					<div class="alert alert-warning" role="alert">過去の時間は入力できません</div>
				</c:if>
				<c:if test="${chkStartFlag}">
					<div class="alert alert-warning" role="alert">正しい日付を入力してください</div>
				</c:if>
				<!-- ▼ アラート表示ここまで -->

				<p>
					<input type="text" required name="start" class="form-control"
						maxlength="16"
						value="<fmt:formatDate value="${event.start}"  pattern="yyyy/MM/dd HH:mm" />" />


				</p>
				<label>終了日時</label>

				<!-- ▼ アラート表示ここから -->
				<c:if test="${endPastTimeAlertFlag}">
					<div class="alert alert-warning" role="alert">過去の時間は入力できません</div>
				</c:if>
				<c:if test="${endCompareStartTimeAlertFlag}">
					<div class="alert alert-warning" role="alert">終了時間が開始時刻より前になっています</div>
				</c:if>
				<c:if test="${chkEndFlag}">
					<div class="alert alert-warning" role="alert">正しい日付を入力してください</div>
				</c:if>
				<!-- ▼ アラート表示ここまで -->

				<p>
					<input type="text" name="end" class="form-control" maxlength="16"
						value="<fmt:formatDate value="${event.end}"  pattern="yyyy/MM/dd HH:mm" />" />
				</p>
				<label>場所 (必須)</label>
				<div class="form-group">
					<select name="room_id" class="form-control">
						<c:forEach items="${rooms}" var="room">


							<option value="<c:out value="${room.id}" />" selected>
								<c:out value="${room.name}" />
							</option>



						</c:forEach>
					</select>
				</div>

				<label>対象グループ</label>

				<div class="form-group">
					<select name="group_name" class="form-control">
						<c:forEach items="${groups}" var="group">


							<c:choose>
								<c:when test="${event.group_id==group.id }">
									<option value="<c:out value="${group.id }" />" selected>
										<c:out value="${group.group_name }" />
									</option>
								</c:when>
								<c:otherwise>
									<option value="<c:out value="${group.id }" />">
										<c:out value="${group.group_name }" />
									</option>
								</c:otherwise>
							</c:choose>


						</c:forEach>
					</select>
				</div>



				<label>詳細</label>
				<p>
					<textarea name="detail"	class="form-control textarea-width"
					maxlength="500"><c:out value="${event.detail }" /></textarea>
				</p>




				<!-- ▼ イベントの編集のキャンセルボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-default">キャンセル</button>
            -->
				<a href="EventDetailServlet?id=<c:out value="${event_id}"/>"
					class="btn btn-default">キャンセル</a>



				<!-- ▼ イベントの編集の保存ボタン -->


				<button type="submit" name="event_id"
					value="<c:out value="${event_id}" />" class="btn btn-primary"
					id="submit">保存</button>


			</form>


		</article>
		</main>
	</div>




	<!-- ▼ フッタ -->






	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>





</body>
</html>
