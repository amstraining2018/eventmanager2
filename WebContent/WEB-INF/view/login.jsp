<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>イベント管理</title>
</head>
<body class="login-body">
	<div class="wrapper login-wrapper container">
		<!-- ▼ メインコンテンツ -->
		<main class="login-contents col-sm-8 col-sm-offset-2 col-xs-12">
		<div class="account-wall">
			<div class="chase-logo">
				<img class="profile-img" src="" alt="">
				<c:if test="${not empty error}">
					<div class="alert alert-warning">ログインIDかパスワードが正しくありません</div>
				</c:if>
			</div>
		</div>
		<article>
			<div class="panel login-panel panel-default">
				<div class="panel-heading">Event Manager</div>
				<div class="panel-body">
					<!-- ▼ 入力フォーム -->
					<form action="" method="post">
						<p>
							<input type="text" name="LOGIN_ID" maxlength="10" class="form-control"
								id="LOGIN_ID" required placeholder="ログインID" />
						</p>

						<p>
							<input type="password"  name="LOGIN_PASS" class="form-control"
								id="LOGIN_PASS" required placeholder="パスワード" />
						</p>

						<p>
							<!-- ▼ ログインボタン -->

							<button type="submit" name="" value=""
								class="btn btn-block btn-primary">ログイン</button>

							<!--  <a href="todaysEvent.jsp" class="btn btn-block btn-primary">ログイン</a>-->

						</p>
					</form>
				</div>
			</div>
		</article>
		</main>
	</div>
</body>
</html>
