<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setAttribute("login_id", request.getAttribute("login_id"));
%>

<!DOCTYPE html>
<html lang="ja">
<head>
<link rel="stylesheet" type="text/css"
	href="https://www.huangwx.cn/css/sweetalert.css">
<script type="text/javascript"
	src="https://www.huangwx.cn/js/sweetalert-dev.js"></script>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="3" /></jsp:include>

		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1"> <img
			class="profile-img" src="" alt=""> <br>
		<br>
		<br>
		<c:if test="${not empty errmsg}">
			<div class="alert alert-warning">旧パスワードが正しくありません</div>
		</c:if> <c:if test="${not empty errmsg2}">
			<div class="alert alert-warning">新旧のパスワードが同じです</div>
		</c:if> <c:if test="${not empty errmsg3}">
			<div class="alert alert-warning">再入力のパスワードが正しくありません</div>
		</c:if>
		<article>
			<h2>ユーザ編集</h2>
			<!-- ▼ 入力フォーム -->
			<form name="form" action="UserPasswordServlet" method="post"
				onsubmit="return userCheck()">

				<label>旧パスワード </label>
				<p>
					<input type="password" required name="oldLogin_pass"
						class="form-control" maxlength="12" />
				</p>
				<label>新パスワード </label>
				<p>
					<input type="password" required name="newLogin_pass"
						pattern="^[0-9A-Za-z-_]{8,12}$"
						title="8文字以上12文字以下で半角英数、ハイフン（ - ）、アンダーバー（ _ ）のみ受付可能"
						 class="form-control" value="<c:out value="login_pass"/>"/>
				</p>
				<label>再入力パスワード </label>
				<p>
					<input type="password" required name="saiLogin_pass"
						class="form-control" maxlength="12"  />

				</p>


				<a href="EventServlet?id=<c:out value="${user.id }" />"
					class="btn btn-default">キャンセル</a>
				<!-- ▼ ユーザ編集の保存ボタン -->

				<button type="submit" name="" value="${user.id }"
					class="btn btn-primary">保存</button>

			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document.write('<script src="js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/dropdown.js"></script>
	<script>
		function userCheck() {

			if (document.form.group_name.value == "") {
				swal("グループ名を選んでてください‼");
				return false
			}
		}
	</script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>


</body>

</html>
