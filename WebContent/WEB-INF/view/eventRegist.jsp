<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<%-- ▼ Internet Explorer … 表示モード-＞常に標準モード --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 --%>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<%-- ▼ Bootstrap --%>
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--  -->

<br>
<br>
<br>
<title>イベント管理</title>
</head>
<%-- コタキスクリプト --%>
<script>
	$(document).ready(
			function() {

				$('#submit').click(
						function() {
							if ($('input[name="namet"]').val() == ""
									| $('input[name="names"]').val() == ""
									| $('input[name="namep"]').val() == "") {
								swal("必須項目を入力してください");
							} else {
								$("#submit").attr("type", "submit");
								$('form').submit();
							}
							;
						});
			});
</script>

<body>
	<div class="wrapper container">
		<%-- ▼ ヘッダ --%>
		<jsp:include page="header.jsp">
			<jsp:param name="kanri" value="2" /></jsp:include>

		<%-- ▼ メインコンテンツ --%>
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>イベント登録</h2>
			<%-- ▼ 入力フォーム --%>
			<form action="EventRegistServlet" method="post">
				<%-- ▼ 情報表示 --%>
				<label>タイトル (必須)</label>
				<p>
					<input type="text" required name="namet" class="form-control"
						placeholder=""  maxlength="50"/>

				</p>
				<label>開始日時 (必須)</label>
				<p>
					<input type="text" required name="names" class="form-control"
						placeholder="2018/01/01 00:00" maxlength="16" />
				</p>

				<%-- ▼ アラート表示ここから --%>
<c:if test="${startPastTimeAlertFlag}">
					<div class="alert alert-warning" role="alert">過去の時間は入力できません</div>
				</c:if>
				<c:if test="${chkStartFlag}">
					<div class="alert alert-warning" role="alert">正しい日付を入力してください</div>
				</c:if>
				<%-- ▼ アラート表示ここまで --%>

				<label>終了日時</label>
				<p>
					<input type="text" name="namef" class="form-control"
						placeholder="2018/01/01 00:00" maxlength="16"  />
				</p>
				<c:if test="${chkEndFlag}">
					<div class="alert alert-warning" role="alert">正しい日付を入力してください</div>
				</c:if>
				<%-- ▼ アラート表示ここから --%>
				<c:if test="${endPastTimeAlertFlag}">
					<div class="alert alert-warning" role="alert">過去の時間は入力できません</div>
				</c:if>
				<c:if test="${endCompareStartTimeAlertFlag}">
					<div class="alert alert-warning" role="alert">終了時間が開始時刻より前になっています</div>
				</c:if>
				<%-- ▼ アラート表示ここまで --%>
				<%-- ▼ アラート表示ここまで --%>
				<label>場所 (必須)</label>
				<div>
					<p>
						<select tabindex="" class="form-control"
							selected="${user.group_name}" name="room_name">
							<span class="input-room-btn">
								<button class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right">
									<c:forEach items="${roomIdList}" var="types" varStatus="loop">
										<option value="${roomNameList[loop.count-1]}">
											${roomNameList[loop.count-1]}</option>
									</c:forEach>
								</ul>
						</span>
						</select>
					</p>
				</div>


				<label>対象グループ</label>
				<div>
					<p>
						<select tabindex="" class="form-control"
							selected="${user.group_name}" name="group_name">
							<span class="input-group-btn">
								<button class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right">
									<c:forEach items="${typesidList}" var="types" varStatus="loop">
										<option value="${typesList[loop.count-1]}">
											${typesList[loop.count-1]}</option>
									</c:forEach>
								</ul>
						</span>
						</select>
					</p>
				</div>

				<label>詳細</label>
				<p>
					<textarea name="namer" class="form-control textarea-width" maxlength="500" ></textarea>
				</p>
				<%-- ▼ イベントの登録キャンセルボタン --%>
				<%--
            <button type="submit" name="" value="" class="btn btn-default">キャンセル</button>
            --%>
				<a href="EventServlet" class="btn btn-default">キャンセル</a>
				<%-- ▼ イベントの登録ボタン --%>

				<button type="submit" id="submit" value="" class="btn btn-primary">登録</button>

				<%-- <a href="../EvenyRegistServlet" class="btn btn-primary">登録</a>  --%>
			</form>
		</article>
		</main>
	</div>
	<%-- ▼ フッタ --%>
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>

</body>
</html>
