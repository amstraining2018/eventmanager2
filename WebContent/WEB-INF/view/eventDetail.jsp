<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 日時表示用フォーマット -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<%-- ▼ Internet Explorer … 表示モード-＞常に標準モード --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 --%>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->


<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css" rel="stylesheet" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<br>
<br>
<br>
<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<%-- ▼ ヘッダ --%>
<jsp:include page="header.jsp"><jsp:param name="kanri" value="2" /></jsp:include>

		<%-- ▼ メインコンテンツ --%>
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>イベント詳細</h2>
			<%-- ▼ 入力フォーム --%>
		<!-- <form action="" method="post"> -->
				<%-- ▼ 情報表示 --%>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<td><b>タイトル</b></td>
								<td><c:out value="${eventList[0].title}" />
								<!-- ▼ 参加ラベル (※自分が参加するイベントの場合) -->
										<c:if test="${sankasinaiButton==true}">
										<span class="label label-danger">参加</span></c:if></td>

							</tr>
							<tr>
							 <fmt:formatDate value="${eventList[0].start}" pattern="yyyy年MM月dd日（E） HH時mm分" var="start" />
								<td><b>開始日時</b></td>
								<td><c:out value="${start}" /></td>

							</tr>
							<tr>
								<td><b>終了日時</b></td>
								 <fmt:formatDate value="${eventList[0].end}" pattern="yyyy年MM月dd日（E） HH時mm分" var="end" />
								<td><c:out value="${end}" /></td>

							</tr>
							<tr>
								<td><b>場所</b></td>

								<td><c:out value="${eventList[0].place}" /></td>

							</tr>
							<tr>
								<td><b>対象グループ</b></td>

								<td><c:out value="${eventList[0].group_name}" /></td>

							</tr>
							<tr>
								<td><b>詳細</b></td>

								<td><c:out value="${eventList[0].detail}" /></td>

							</tr>
							<tr>
								<td><b>登録者</b></td>

								<td><c:out value="${eventList[0].registered_name}" /></td>

							</tr>
							<tr>
								<td><b>参加者</b></td>
								<td>
								<c:forEach items = "${AttendsIdList}" var="attends">
								<c:out value="${attends.name}" />
								</c:forEach>
								</td>

							</tr>
					</table>
				</div>
				<%-- ▼ 一覧に戻るボタン --%>
				<%--
            <button type="submit" name="" value="" class="btn btn-primary">一覧に戻る</button>
            --%>
				<a href="EventServlet" class="btn btn-primary">一覧に戻る</a>
				<%-- ▼ 参加するボタン (※自分が参加しないイベントの場合) --%>

				<c:if test="${sankasinaiButton==false}">
					<a
						href="EventSubmitServlet?event_id=<c:out value="${event_id}"/>&user_id=<c:out value="${user_id}"/>"
						class="btn btn-info">参加する</a>
				</c:if>

				<%-- ▼ 参加を取り消すボタン (※自分が参加するイベントの場合) --%>


				<c:if test="${sankasinaiButton==true}">
						<a
						href="EventAttendsDeleteServlet?event_id=<c:out value="${event_id}"/>&user_id=<c:out value="${user_id}"/>"
						class="btn btn-warning">参加を取り消す</a>
				</c:if>

				<%-- ▼ 編集ボタン (※自分が登録したイベント／管理ユーザの場合) --%>
				<c:if test="${EditAndDeleteButton==true}">
				<a href="EventEditServlet?id=<c:out value="${event_id}"/>" class="btn btn-default">編集</a>
				</c:if>


			<!-- ▼ 削除ボタン (※自分が登録したイベント／管理ユーザの場合) -->
			<!--
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmModal">削除</button>

            <div class="modal fade" id="confirmModal" tabindex="-1">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                    <p>本当に削除してよろしいですか？</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                    <a href="eventDeleteComp.html" class="btn btn-primary">OK</a>
                  </div>
                </div>
              </div>
            </div>
 -->

				<%-- ▼ 削除ボタン (※自分が登録したイベント／管理ユーザの場合) --%>
				<c:if test="${EditAndDeleteButton==true}">
				<button type="button" class="btn btn-danger" data-toggle="modal"
					data-target="#confirmModal">削除</button>
					</c:if>
				<%-- ▼ モーダル・ダイアログ --%>
				<div class="modal fade" id="confirmModal" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal">
									<span>×</span>
								</button>
								<p>本当に削除してよろしいですか？</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Cancel</button>
						 <form action="EventDeleteServlet" method="post">
								<button type="submit" class="btn btn-primary">OK</button>


									<input type="hidden" name="id" value="${event_id}">


							</form>



								<%--
                    <button type="submit" name="" value="" class="btn btn-primary">OK</button>
                    --%>
			<!-- 				<a href="EventDeleteServlet?id=<c:out value="${event_id}"/>" class="btn btn-primary">OK</a> -->
							</div>
						</div>
					</div>
				</div>
	<!-- 	</form> -->
		</article>
		</main>
	</div>

	<%-- ▼ フッタ --%>
	<footer class="footer">

		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
</body>
</html>
