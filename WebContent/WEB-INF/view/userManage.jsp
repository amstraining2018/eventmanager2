<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	//表示するページ
	int nowPageNumber = (Integer) request.getAttribute("nowPageNumber");

	//全ページ数
	int allPageNumber = (Integer) request.getAttribute("allPageNumber");
%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="3" /></jsp:include>


		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<br> <br> <br>
			<h2>ユーザ一覧</h2>
			<!-- ▼ 入力フォーム -->
<c:if test="${not empty err}">
				<div class="alert alert-warning">
					<c:out value="${err}"/>
				</div>
				</c:if>
			<form action="" method="post">







				<!-- ▼ ページ遷移 -->
				<nav class="clearfix">
					<ul class="pagination pull-right">

						<c:if test="${nowPageNumber!=1}">
							<li><a
								href="UserManageServlet?nowPageNumber=<c:out value="${1}" />"
								aria-label="前のページへ"> <span aria-hidden="true">≪</span>
							</a></li>
						</c:if>


						<c:forEach begin="1" end="${allPageNumber}" varStatus="status">

							<c:choose>
								<c:when test="${status.index==nowPageNumber}">
									<li class="active"><a
										href="UserManageServlet?nowPageNumber=<c:out value="${status.index}" />">
											<c:out value="${status.index}" />
									</a></li>
								</c:when>

								<c:when test="${status.index!=nowPageNumber}">
									<li><a
										href="UserManageServlet?nowPageNumber=<c:out value="${status.index}" />">
											<c:out value="${status.index}" />
									</a></li>
								</c:when>
							</c:choose>
						</c:forEach>

						<c:if test="${nowPageNumber!=allPageNumber}">
							<li><a
								href="UserManageServlet?nowPageNumber=<c:out value="${allPageNumber}" />"
								aria-label="次のページへ"> <span aria-hidden="true">≫</span>
							</a></li>
						</c:if>

					</ul>
				</nav>
				<!-- 登録されていない社員がいないと -->
				<%
					String flg = (String) request.getAttribute("flg");
					if (flg.equals("0")) {
				%>
				<p class="text-danger">登録されていない社員はいません</p>
				<%
					}
				%>

				<!-- ▼ 情報表示 -->
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>ID</th>
								<th>氏名</th>
								<th>所属グループ</th>
								<th>社員ID</th>
								<th>詳細</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${userList}" var="user">

								<tr>
									<td><c:out value="${user.id }" /></td>
									<td><c:out value="${user.name }" /></td>
									<td><c:out value="${user.group_name }" /></td>
									<td><c:out value="${user.employee_id }" /></td>
									<td><a
										href="UserDetailServlet?id=<c:out value="${user.id }" />"
										class="btn btn-default">詳細</a></td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</div>
				<!-- ▼ ユーザの登録ボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-primary">ユーザの登録</button>
            -->
				<a href="userRegist" id="userRegist" class="btn btn-primary">ユーザの登録</a>

				<a href="AccountFileReaderServlet" class="btn btn-primary">ユーザ一括登録</a>

			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>

</body>
</html>
