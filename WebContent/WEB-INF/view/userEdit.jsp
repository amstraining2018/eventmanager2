<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
request.setAttribute("login_id",request.getAttribute("login_id"));

%>

<!DOCTYPE html>
<html lang="ja">
<head>
<link rel="stylesheet" type="text/css"
	href="https://www.huangwx.cn/css/sweetalert.css">
<script type="text/javascript"
	src="https://www.huangwx.cn/js/sweetalert-dev.js"></script>

<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">

<link href="css/style.css" rel="stylesheet" />
<link href="css/sticky-footer.css" rel="stylesheet" />

<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="3" /></jsp:include>

		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>ユーザ編集</h2>
			<!-- ▼ 入力フォーム -->
			<form name="form" action="UserEditComp?id=<c:out value="${user.id }" />" method="post"
				onsubmit="return userCheck()">

				<!-- ▼ 情報表示 -->
				<label>氏名 (必須)</label>

				<p>
					<input type="text" required name="name" class="form-control"
						value="${user.name}" />
				</p>
				<label>ログインID </label>
				<p>
				<input type="hidden" name="login_id" value="${user.login_id}"/>
					<input type="text" class="form-control"  disabled="disabled"
						value="${user.login_id}" />
				</p>
				<label>パスワード (変更の場合のみ) </label>
				<p>
					<input type="password" name="login_pass" class="form-control" />
				</p>



				<label>所属グループ (必須)</label>

				<div>
					<p>
						<select tabindex="" data-placeholder="" class="form-control"
							selected="${user.group_name}" name="group_name">


							<span class="input-group-btn">
								<button class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right">

									<c:forEach items="${typesidList}" var="types" varStatus="loop">
										<option value="${typesidList[loop.count-1]}">
											${typesList[loop.count-1]}</option>




									</c:forEach>
								</ul>
						</span>
						</select>
					</p>
				</div>


				<label>社員ID (必須)</label>
				<p>
					<input type="hidden" class="form-control"
						value="${user.employee_id}" />
				</p>
				<p>
					<input type="text" required name="employee_id" name="employee_id"
						class="form-control" />
				</p>


				<a href="UserDetailServlet?id=<c:out value="${user.id }" />"
					class="btn btn-default">キャンセル</a>
				<!-- ▼ ユーザ編集の保存ボタン -->

				<button type="submit" name="" value="" class="btn btn-primary">保存</button>

			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document.write('<script src="js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/dropdown.js"></script>
	<script>
		function userCheck() {

			if (document.form.name.value == "") {
				swal("名前を入力してください‼");
				return false;
			}
			if (document.form.login_id.value == "") {
				swal("ユーザー名入力してください‼");
				return false
			}
			if (document.form.group_name.value == "") {
				swal("グループ名を選んでてください‼");
				return false
			}
			if (document.form.group_name.value == "") {
				swal("グループ名を選んでてください‼");
				return false
			}

		}
	</script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>


</body>

</html>
