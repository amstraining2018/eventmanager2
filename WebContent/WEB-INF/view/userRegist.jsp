<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<link rel="stylesheet" type="text/css"
	href="https://www.huangwx.cn/css/sweetalert.css">
<script type="text/javascript"
	src="https://www.huangwx.cn/js/sweetalert-dev.js"></script>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer… 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script>
	$(function() {
		//セレクトボックスが切り替わったら発動
		$('select').change(function() {

			//選択したvalue値を変数に格納
			var result = $(this).val().split(',');
			$('#EmployeeName').text(result[1]);
			$('#DepartMentName').text(result[2]);

		});
	});

</script>
<title>イベント管理</title>
</head>
<body>


	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="3" /></jsp:include>

		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<!-- ログインIDのpatternは文字指定 この状態だと0-9、A-Z、a-z、-、_が入力可能 -->
			<br> <br> <br>
			<h2>ユーザ登録</h2>



			<!-- ▼ 入力フォーム -->
			<form action="userRegist" method="post">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="unSetAccountList">社員ID</label>
					<p class="text-danger" id="employee_id_err"></p>
					<p>
						<select class="form-control col-sm-9" id="unSetAccountList"
							name="employee_id_List">
							<c:forEach items="${UserInfoList}" var="User">
								<option
									value="${User.employee_id},${User.employee_name},${User.groups_name}">
									<c:out value="${User.employee_id}" /></option>
							</c:forEach>
						</select>
					</p>
					<%
						int err1 = (Integer) request.getAttribute("err1");
						if (err1 == -1) {
					%>
					<p class="text-danger">社員IDを選択してください</p>
					<%
						}
					%>


					<label class="col-sm-3 control-label">名前</label>
					<p class="form-control col-sm-9" id="EmployeeName" /></p>
					<br> <label class="col-sm-3 control-label">部署名</label>
					<p class="form-control col-sm-9" id="DepartMentName" /></p>




					<label class="col-sm-3 control-label">ログインID（必須）</label>
					<p>
						<input type="text" required name="input_login_id"  maxlength="10" class="form-control"
							pattern="^[0-9A-Za-z-_]+$" placeholder="ログインID"
							title="半角英数、ハイフン（ - ）、アンダーバー（ _ ）のみ受付可能"
							value="<c:out value="${login_id}"/>" />
					</p>
					<%
						int err2 = (Integer) request.getAttribute("err2");
						if (err2 == -1) {
					%>
					<p class="text-danger">すで登録済みのログインIDのため登録ができません。</p>
					<%
						}
					%>


					<label class="col-sm-3 control-label">パスワード（必須）</label>
					<p>
						<input type="password"
						name ="input_logiin_pass"
						pattern="^[0-9A-Za-z]{8,12}$"
							title="8文字以上12文字以下の半角英数のみ受付可能"required name="login_pass"
							class="form-control" placeholder="パスワード"
							value="<c:out value="${login_pass}"/>" />
							<!-- value=以降を"${login_pass}" -->
					</p>
				</div>
				<a href="UserManageServlet" class="btn btn-default">キャンセル</a>
				<button type="submit" id="submit" class="btn btn-primary">登録</button>
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="fixed-bottom">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>

</body>
</html>