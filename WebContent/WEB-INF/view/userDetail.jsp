<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- 一覧ページで選択されたときに使われるスクリプト -->
<script>
	function submitAction(url) {
		$('form').attr('action', url);
		$('form').submit();
	}
</script>
<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp">
			<jsp:param name="kanri" value="3" /></jsp:include>

		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<br> <br> <br>
			<h2>ユーザ詳細</h2>
			<!-- ▼ 入力フォーム -->
			<form action="UserDetailServlet" method="get">
				<!-- ▼ 情報表示 -->
				<div class="table-responsive">
					<table class="table">

						<!--
最初の<td>（ここでいうID）に <div style="margin-right:500px"><c:out value="${user.id}" /></div>を入れたら、
残りのuser.name、user.group_nameのところは変更しなくても反映されて左側に詰めてくれるみたいです。
細かい仕様は知りません。
 -->

						<tr>
							<th>ID</th>
							<td><div style="margin-right: 500px">
									<c:out value="${user.id}" />
								</div></td>
						</tr>
						<tr>
							<th>氏名</th>
							<td><c:out value="${user.name}" /></td>
						</tr>
						<tr>
							<th>所属グループ</th>
							<td><c:out value="${user.group_name}" /></td>
						</tr>
						<tr>
							<th>社員ID</th>
							<td><c:out value="${user.employee_id}" /></td>
						</tr>

					</table>
				</div>
				<!-- ▼ 一覧に戻るボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-primary">一覧に戻る</button>

            -->
				<a href="UserManageServlet" class="btn btn-primary">一覧に戻る</a>
				<!-- ▼ 編集ボタン -->
				<!--

				<!-- ▼ 削除ボタン -->

				<c:if test="${userId!=user.id}">
					<button type="button" class="btn btn-danger" data-toggle="modal"
						data-target="#confirmModal">削除</button>
				</c:if>


				<!-- ▼ モーダル・ダイアログ -->
				<div class="modal fade" id="confirmModal" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal">
									<span>×</span>
								</button>
								<p>本当に削除してよろしいですか？</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Cancel</button>

								<!--  <button type="button" class="btn btn-primary" onclick="submitAction('UserDeleteServlet')">OK</button><-->

								<!--   <a href="UserDeleteServlet" class="btn btn-primary">OK</a><-->

								<button type="submit" class="btn btn-primary" name="delete"
									onclick="submitAction('UserDeleteServlet')" value="${user.id}">OK</button>

								<!-- OKボタンを押すと UserDeleteServletのdo postに飛ぶ-->


							</div>
						</div>
					</div>
				</div>
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
</body>
</html>
