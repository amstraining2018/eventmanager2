<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8"/>
    <!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css" rel="stylesheet" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<br>
<br>
<br>
    <title>イベント管理</title>
  </head>
  <body>
    <div class="wrapper container">
      <!-- ▼ ヘッダ -->
     <jsp:include page="header.jsp"><jsp:param name="kanri" value="2" /></jsp:include>
      <!-- ▼ メインコンテンツ -->
      <main class="main-contents col-sm-10 col-sm-offset-1">
        <article>
          <h2>イベント登録</h2>
          <p>イベントの登録が完了しました。</p>
          <a href="EventServlet">イベント一覧に戻る</a>
        </article>
      </main>
    </div>
    <!-- ▼ フッタ -->
    <footer class="footer">
      <p class="text-center text-muted">© icloud 2018 Inc.</p>
    </footer>
  </body>
</html>
