<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 日時表示用フォーマット -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- ページを受け取るためのもの -->
<%
	request.setCharacterEncoding("UTF-8");
	String NAME = (String) request.getAttribute("NAME");
	int current_page_number = (Integer) request.getAttribute("current_page_number");
	int pages = (Integer) request.getAttribute("pages");/* 現在いるページ */
%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sticky-footer/">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="1" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
		<br><br><br>
			<h2>本日のイベント</h2>
			<!-- ▼ 入力フォーム -->
			<form action="" method="post">
				<!-- ▼ ページ遷移 -->
				<!-- 前のページへ -->
				<nav class="clearfix">
					<ul class="pagination pull-right">
						<c:if test="${CurrentPageNumber!=1}">
							<li><a
								href="TodaysEventServlet?CurrentPageNumber=<c:out value="${1}" />"
								aria-label="前のページへ"> <span aria-hidden="true">≪</span>
							</a></li>
						</c:if>
						<!-- 1~nページまで遷移 -->
						<c:forEach begin="1" end="${pages}" varStatus="status">
							<c:choose>
								<c:when test="${status.index==CurrentPageNumber}">
									<li class="active"><a
										href="TodaysEventServlet?CurrentPageNumber=<c:out value="${status.index}" />">
											<c:out value="${status.index}" />
									</a></li>
								</c:when>
								<c:when test="${status.index!=CurrentPageNumber}">
									<li><a
										href="TodaysEventServlet?CurrentPageNumber=<c:out value="${status.index}" />">
											<c:out value="${status.index}" />
									</a></li>
								</c:when>
							</c:choose>
						</c:forEach>
						<!-- 次のページ -->
						<c:if test="${CurrentPageNumber!=pages}">
							<li><a
								href="TodaysEventServlet?CurrentPageNumber=<c:out value="${pages}" />"
								aria-label="次のページへ"> <span aria-hidden="true">≫</span>
							</a></li>
						</c:if>
					</ul>
				</nav>





				<!-- ▼ 情報表示 -->
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>タイトル</th>
								<th>開始日時</th>
								<th>場所</th>
								<th>対象グループ</th>
								<th>詳細</th>
							</tr>
						</thead>
						<tbody>
							<!-- show 1 line  -->
							<c:forEach items="${eventList}" var="event">
								<tr>
									<td><c:out value="${ event.title }" /> <c:forEach
											items="${attendsList}" var="attends">
											<c:if test="${event.id == attends.event_id}">
												<span class="label label-danger">参加</span>
												<%-- cタグはif文内では使えません  <c:out value="${参加}" /> --%>
											</c:if>
										</c:forEach></td>
									<fmt:formatDate value="${event.start}"
										pattern="yyyy年MM月dd日（E） HH時mm分" var="date" />
									<td><c:out value="${date}" /></td>
									<td><c:out value="${event.place}" /></td>
									<td><c:out value="${event.group_name}" /></td>


									<td>
										<!--

                      --> <a
										href="EventDetailServlet?id=<c:out value="${event.id}" />"
										class="btn btn-default">詳細</a>
									</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</div>
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
</body>
</html>
