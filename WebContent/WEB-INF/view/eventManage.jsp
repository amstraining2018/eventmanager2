<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	//表示するページ
	int nowPageNumber = (Integer) request.getAttribute("nowPageNumber");

	//全ページ数
	int allPageNumber = (Integer) request.getAttribute("allPageNumber");

	//csv出力のぼたんを表示するか否かぱらめーたーをString型でとってくる
	int type_id = (Integer) request.getAttribute("type_id");
%>
<!-- 日時表示用フォーマット -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="ja">
<!--  -->
<head>
<meta charset="utf-8" />
<%-- ▼ Internet Explorer … 表示モード-＞常に標準モード --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 --%>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<%-- ▼ Bootstrap --%>
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
	function clickBtn1() {
		const num = document.formGroup.group_id.selectedIndex;
		const num2 = document.formGroup.room_id.selectedIndex;
		const str = document.formGroup.group_id.options[num].value;
		const str2 = document.formGroup.room_id.options[num2].value;

		document.outputFrmCsv.selectGrp.value = str;
		document.outputFrmCsv.selectRm.value = str2;
	}
</script>

<br>
<br>
<br>
<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<%-- ▼ ヘッダ --%>
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="2" /></jsp:include>
		<%-- ▼ メインコンテンツ --%>
		<main class="main-contents col-sm-10 col-sm-offset-1">

		<article>
			<h2>イベント一覧</h2>

						<c:if test="${not empty errmsg1}">
				<div class="alert alert-warning">
					<c:out value="${errmsg1}"/>
				</div>
			</c:if>
			<c:if test="${not empty msg}">
				<div class="alert alert-warning">
					<c:out value="${msg}"/>
				</div>
			</c:if>
			<c:if test="${not empty errmsg}" >
			<div class="alert alert-warning" ><c:out value="${errmsg }" /></div>
			</c:if>

			<br> <br>

			<%-- ▼ 入力フォーム --%>
			<div style="display: inline-flex">
				<form action="EventSearchServlet" method="post" class="form-inline"
					name="formGroup">
					<label>所属</label> <select title="グループ指定検索" style="width: 250px"
						name="group_id" class="form-control">
						<c:forEach items="${groups}" var="group">
							<c:choose>
								<c:when test="${groupId==group.id }">
									<option value="<c:out value="${groupId }" />" selected>
										<c:out value="${group.group_name }" />
									</option>
								</c:when>
								<c:otherwise>
									<option value="<c:out value="${group.id }" />">
										<c:out value="${group.group_name }" />
									</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select> <label> &ensp;会議室</label> <select title="場所指定検索"
						style="width: 250px" name="room_id" class="form-control">


						<c:forEach items="${rooms}" var="room">
							<c:choose>
								<c:when test="${roomId==room.id }">
									<option value="<c:out value="${roomId}" />" selected>
										<c:out value="${room.name}" />
									</option>
								</c:when>
								<c:otherwise>
									<option value="<c:out value="${room.id}" />">
										<c:out value="${room.name}" />
									</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select> <input type="submit" value="検索" class="btn btn-info"
						style="width: 100px">



				</form>
				<!-- 受け取ったぱらめーたで表示非表示を判定ー -->
				<%
					if (type_id == 2) {
				%>
				<button type="button" class="btn btn-success" data-toggle="modal"
					data-target="#confirmModal" style="width: 100px">CSV出力</button>

				<%
					}
				%>

				<div class="modal fade" id="confirmModal" tabindex="-1">

					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal">
									<span>×</span>
								</button>
								<p>上書きしてよろしいですか？</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Cancel</button>
								<form action="EventServlet" method="post" name="outputFrmCsv"
									onclick="clickBtn1()">
									<input type="hidden" name="selectGrp"> <input
										type="hidden" name="selectRm">
									<button type="submit" class="btn btn-primary">OK</button>

								</form>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!--  -->




			<%-- ▼ ページ遷移 --%>
			<nav class="clearfix">
				<ul class="pagination pull-right">

					<c:if test="${nowPageNumber!=1}">
						<li><a
							href="EventServlet?nowPageNumber=<c:out value="${1}" />"
							aria-label="前のページへ"> <span aria-hidden="true">≪</span>
						</a></li>
					</c:if>


					<c:forEach begin="1" end="${allPageNumber}" varStatus="status">

						<c:choose>
							<c:when test="${status.index==nowPageNumber}">
								<li class="active"><a
									href="EventServlet?nowPageNumber=<c:out value="${status.index}" />">
										<c:out value="${status.index}" />
								</a></li>
							</c:when>

							<c:when test="${status.index!=nowPageNumber}">
								<li><a
									href="EventServlet?nowPageNumber=<c:out value="${status.index}" />">
										<c:out value="${status.index}" />
								</a></li>
							</c:when>
						</c:choose>
					</c:forEach>

					<c:if test="${nowPageNumber!=allPageNumber}">
						<li><a
							href="EventServlet?nowPageNumber=<c:out value="${allPageNumber}" />"
							aria-label="次のページへ"> <span aria-hidden="true">≫</span>
						</a></li>
					</c:if>

				</ul>
			</nav>

			<%-- ▼ 情報表示 --%>
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>タイトル</th>
							<th>開始日時</th>
							<th>場所</th>
							<th>対象グループ</th>
							<th>詳細</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${eventList }" var="event">
							<tr>
								<td><c:out value="${event.title}" /> <c:forEach
										items="${attendsList}" var="attends">
										<c:if test="${event.id == attends.event_id}">
											<span class="label label-danger">参加</span>
										</c:if>
									</c:forEach></td>
								<fmt:formatDate value="${event.start}"
									pattern="yyyy年MM月dd日（E） HH時mm分" var="date" />
								<td><c:out value="${date}" /></td>
								<td><c:out value="${event.place}" /></td>
								<td><c:out value="${event.group_name}" /></td>
								<td>
									<%--
                      <button type="submit" name="" value="" class="btn btn-default">詳細</button>
--%> <a href="EventDetailServlet?id=<c:out value="${event.id}" />"
									class="btn btn-default">詳細</a>

								</td>
							</tr>


						</c:forEach>
					</tbody>
				</table>
			</div>
			<%-- ▼ イベントの登録ボタン --%>
			<%--
            <button type="submit" name="" value="" class="btn btn-primary">イベントの登録</button>
            --%>
            			<c:if test="${not empty msg}">
				<div class="alert alert-warning">
					<c:out value="${msg}"/>
				</div>
			</c:if>
			<a href="EventRegistServlet" class="btn btn-primary">イベントの登録</a>
			<!-- 管理者のみ場所の一括登録が可能 -->
			<%
			if (type_id == 2) {
			%>
			<a href="FileReaderServlet" class="btn btn-primary">場所一括登録</a>
			<%
					}
			%>


			<a href="UserPasswordServlet" class="btn btn-primary">ユーザーパスワード変更</a>


		</article>
		</main>
	</div>
	<%-- ▼ フッタ --%>
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
</body>
</html>
