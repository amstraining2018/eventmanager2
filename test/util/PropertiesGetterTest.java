package util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

import org.junit.Test;

import util.PropertiesGetter;

public class PropertiesGetterTest {
	public static final String PATH = "c:\\users\\user\\documents\\room_20180601.csv";
	public static final String PATH2 = "C:\\Users\\user\\Documents\\account_20180601.csv";
	public static final String PATH3 = "C:\\Users\\user\\Documents";
	private static final String ADDRESS = "C:\\pleiades\\workspace\\eventmanager2\\src\\fileRead.properties";
	private static final String WADDRESS = "C:\\\\pleiades\\\\workspace\\\\eventmanager2\\\\src\\\\fileRead.properties_";

	@Test
	public void roomファイル取得確認testGetProperties() throws Exception {
		PropertiesGetter p = new PropertiesGetter();
		String expect = PATH;
		String result = p.getProperties("room", ADDRESS);
		assertThat(result, is(expect));

	}

	@Test
	public void accountファイル取得確認testGetProperties() throws Exception {
		PropertiesGetter p = new PropertiesGetter();
		String expect = PATH2;
		String result = p.getProperties("account", ADDRESS);
		assertThat(result, is(expect));

	}

	@Test
	public void outputファイル取得確認testGetProperties() throws Exception {
		PropertiesGetter p = new PropertiesGetter();
		String expect = PATH3;
		String result = p.getProperties("output", ADDRESS);
		assertThat(result, is(expect));
	}

	@Test(expected = IOException.class)
	public void IOExceptionを出す() throws Exception {
		PropertiesGetter p = new PropertiesGetter();
		p.getProperties("room", WADDRESS);

	}

	@Test(expected = NoSuchFileException.class)
	public void FNFExceptionを出す() throws Exception {
		PropertiesGetter p = new PropertiesGetter();
		p.getProperties("room", WADDRESS);
	}

}
