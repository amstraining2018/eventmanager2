package util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import domain.User;

public class AccountFileReadMethodTest {
	private final static String ADDRESS="C:\\Users\\user\\Documents\\test\\account_20180624.csv";
	private final static String ADDRESS1="C:\\Users\\user\\Documents\\test\\account_20180624(1).csv";//アルファベット未入力S
	private final static String ADDRESS2="C:\\Users\\user\\Documents\\test\\account_20180624(2).csv";//H
	private final static String ADDRESS3="C:\\Users\\user\\Documents\\test\\account_20180624(3).csv";//DE
	private final static String ADDRESS4="C:\\Users\\user\\Documents\\test\\account_20180624(4).csv";//アルファベット間違いS
	private final static String ADDRESS5="C:\\Users\\user\\Documents\\test\\account_20180624(5).csv";//A
	private final static String ADDRESS6="C:\\Users\\user\\Documents\\test\\account_20180624(6).csv";//DをEに
	private final static String ADDRESS7="C:\\Users\\user\\Documents\\test\\account_20180624(7).csv";//時間
	private final static String ADDRESS8="C:\\Users\\user\\Documents\\test\\account_20180624(8).csv";//アルファベットDをHに
	private final static String ADDRESS9="C:\\Users\\user\\Documents\\test\\account_20180624(9).csv";//ヘッダー要素数
	private final static String ADDRESS10="C:\\Users\\user\\Documents\\test\\account_20180624(10).csv";//データ要素数
	private final static String ADDRESS11="C:\\Users\\user\\Documents\\test\\account_20180624(11).csv";//ID
	private final static String ADDRESS12="C:\\Users\\user\\Documents\\test\\account_20180624(12).csv";//アカウント
	private final static String ADDRESS13="C:\\Users\\user\\Documents\\test\\account_20180624(13).csv";//パスワード7
	private final static String ADDRESS14="C:\\Users\\user\\Documents\\test\\account_20180624(14).csv";//権限
	private final static String ADDRESS15="C:\\Users\\user\\Documents\\test\\account_20180624(15).csv";//カウント未入力
	private final static String ADDRESS16="C:\\Users\\user\\Documents\\test\\account_20180624(16).csv";//カウント不正
	private final static String ADDRESS17="C:\\Users\\user\\Documents\\test\\account_20180624(17).csv";//ファイルなし

	private final static String ADDRESS18="C:\\Users\\user\\Documents\\test\\account_20180624(18).csv";//パスワード13
	private final static String ADDRESS19="C:\\Users\\user\\Documents\\test\\account_20180624(19).csv";//日付未入力
	private final static String ADDRESS20="C:\\Users\\user\\Documents\\test\\account_20180624(20).csv";//カウント間違い
	private final static String ADDRESS21="C:\\Users\\user\\Documents\\test\\account_20180624(21).csv";//カウント間違い
	@Test
	public void test() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		List<User> ret =accountFileReadMethod.readCsvFile(ADDRESS);
		List<User> success = addlist();;
		for(int i=0;i<ret.size();i++) {
			assertThat(ret.get(i).getEmployee_id(),is(success.get(i).getEmployee_id()));
			assertThat(ret.get(i).getLogin_id(),is(success.get(i).getLogin_id()));
			assertThat(ret.get(i).getType_id(),is(success.get(i).getType_id()));
		}
	}
	private List<User> addlist(){
		List<User> AddList = new ArrayList<>();
		User user = new User();
		user.setEmployee_id("105");
		user.setLogin_id("a-yamamoto");
		user.setType_id(2);
		AddList.add(user);

		User user2 = new User();
		user2.setEmployee_id("106");
		user2.setLogin_id("y-nakamura");
		user2.setType_id(2);
		AddList.add(user2);

		return AddList;

	}

	@Test
	public void csv未入力アルファベットS() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS1);
		String result=accountFileReadMethod.err;
		String success = "CSVファイルに不正があります：S,H,D,Eのいずれかの文字が入っていませんS";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv未入力アルファベットH() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS2);
		String result=accountFileReadMethod.err;
		String success = "CSVファイルに不正があります：S,H,D,Eのいずれかの文字が入っていませんH";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv未入力アルファベットDE() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS3);
		String result=accountFileReadMethod.err;

		String success = "CSVファイルに不正があります：S,H,D,Eのいずれかの文字が入っていませんD,E";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv不正アルファベットS() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS4);
		String result=accountFileReadMethod.err;

		String success = "CSVファイルに不正があります：Sが入っていません";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv不正アルファベットH() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS5);
		String result=accountFileReadMethod.err;

		String success = "CSVファイルに不正があります：Hが入っていません";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv不正アルファベットDをEに() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS6);
		String result=accountFileReadMethod.err;

		String success = "レコードが不正です";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv時間フォーマット() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS7);
		String result=accountFileReadMethod.err;

		String success = "CSVファイルに不正があります：時間がフォーマット通りではない、またはnullです";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv不正アルファベットDE2() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS8);
		String result=accountFileReadMethod.err;
		String success = "正しいアルファベットが入力されていません";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvヘッダー要素数() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS9);
		String result=accountFileReadMethod.err;

		String success = "CSVファイルに不正があります：ヘッダの数が要素数と合致していません";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvデータ要素数() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS10);
		String result=accountFileReadMethod.err;

		String success = "CSVファイルに不正があります：データの数が要素数と合致していません";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv社員ID不正() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS11);
		String result=accountFileReadMethod.err;

		String success = "社員IDに不正があります";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvアカウント不正() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS12);
		String result=accountFileReadMethod.err;

		String success = "アカウントに不正があります";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvパスワード不正7() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS13);
		String result=accountFileReadMethod.err;
		String success = "パスワードに不正があります";
		assertThat(result,is(success));
		System.out.println(success);
		}


	@Test
	public void csv権限不正() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS14);
		String result=accountFileReadMethod.err;
		String success = "権限に不正があります";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvカウント未入力() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS15);
		String result=accountFileReadMethod.err;
		String success = "レコードが不正です";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvカウントなし() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS16);
		String result=accountFileReadMethod.err;
		String success = "レコードに数字が入力されていません";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvファイルなし() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS17);
		String result=accountFileReadMethod.err;
		String success = "ファイルが存在しません";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvパスワード不正13() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS18);
		String result=accountFileReadMethod.err;
		String success = "パスワードに不正があります";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv時間なし() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS19);
		String result=accountFileReadMethod.err;

		String success = "CSVファイルに不正があります：時間がフォーマット通りではない、またはnullです";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csvカウント間違い() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS20);
		String result=accountFileReadMethod.err;
		String success = "CSVファイルに不正があります";
		assertThat(result,is(success));
		System.out.println(success);
		}
	@Test
	public void csv日付フォーマット間違い() {
		AccountFileReadMethod accountFileReadMethod = new AccountFileReadMethod();
		accountFileReadMethod.readCsvFile(ADDRESS21);
		String result=accountFileReadMethod.err;
		String success = "日付のフォーマットに誤りがあります";
		assertThat(result,is(success));
		System.out.println(success);
		}


}
