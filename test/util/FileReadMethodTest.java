package util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import domain.Room;

public class FileReadMethodTest {
	public static final String PATH = "C:\\Users\\user\\Documents\\room_20180601.csv";
	public static final String WPATH = "C:\\Users\\user\\Documents\\room_20180601.csv_";
	public static final String PATH1 = "C:\\Users\\user\\Documents\\room_20180601-1.csv";
	public static final String PATH2 = "C:\\Users\\user\\Documents\\room_20180601-2.csv";
	public static final String PATH3 = "C:\\Users\\user\\Documents\\room_20180601-3.csv";
	public static final String PATH4 = "C:\\Users\\user\\Documents\\room_20180601-4.csv";
	public static final String PATH5 = "C:\\Users\\user\\Documents\\room_20180601-5.csv";
	public static final String PATH6 = "C:\\Users\\user\\Documents\\room_20180601-6.csv";
	public static final String PATH7 = "C:\\Users\\user\\Documents\\room_20180601-7.csv";
	public static final String PATH8 = "C:\\Users\\user\\Documents\\room_20180601-8.csv";
	public static final String PATH9 = "C:\\Users\\user\\Documents\\room_20180601-9.csv";
	public static final String PATH10 = "C:\\Users\\user\\Documents\\room_20180601-10.csv";
	public static final String PATH11 = "C:\\Users\\user\\Documents\\room_20180601-11.csv";
	public static final String PATH12 = "C:\\Users\\user\\Documents\\room_20180601-12.csv";
	public static final String PATH13 = "C:\\Users\\user\\Documents\\room_20180601-13.csv";
	public static final String PATH14 = "C:\\Users\\user\\Documents\\room_20180601-14.csv";
	public static final String PATH15 = "C:\\Users\\user\\Documents\\room_20180601-15.csv";
	public static final String PATH16 = "C:\\Users\\user\\Documents\\room_20180601-16.csv";
	public static final String PATH17 = "C:\\Users\\user\\Documents\\room_20180601-17.csv";
	public static final String PATH18 = "C:\\Users\\user\\Documents\\room_20180601-18.csv";
	public static final String PATH19 = "C:\\Users\\user\\Documents\\room_20180601-19.csv";
	public static final String PATH20 = "C:\\Users\\user\\Documents\\room_20180601-20.csv";

	public static final String[][] roomList = { { "第一会議室", "10", "1", "1", "1", "17:00" },
			{ "第二会議室", "20", "0", "1", "0", "19:00" }, { "第三会議室", "30", "1", "1", "0", "20:00" } };

	@Test
	public void 正常系確認testReadCsvFile() throws Exception {
		List<Room> EXPECT = new ArrayList<>();

		for (int i = 0; i < 3; i++) {
			Room room = new Room();

			room.setName(roomList[i][0]);
			room.setCapacity(Integer.parseInt(roomList[i][1]));
			room.setMic(Integer.parseInt(roomList[i][2]));
			room.setBoard(Integer.parseInt(roomList[i][3]));
			room.setProjector(Integer.parseInt(roomList[i][4]));
			room.setTime(roomList[i][5]);

			EXPECT.add(room);

		}

		FileReadMethod f = new FileReadMethod();

		List<Room> result = f.readCsvFile(PATH);

		//		int cnt = 0;

		for (int i = 0; i < result.size(); i++) {
			assertThat(result.get(i).getName(), is(EXPECT.get(i).getName()));
			assertThat(result.get(i).getCapacity(), is(EXPECT.get(i).getCapacity()));
			assertThat(result.get(i).getMic(), is(EXPECT.get(i).getMic()));
			assertThat(result.get(i).getBoard(), is(EXPECT.get(i).getBoard()));
			assertThat(result.get(i).getProjector(), is(EXPECT.get(i).getProjector()));
			assertThat(result.get(i).getTime(), is(EXPECT.get(i).getTime()));

			//			assertThat(result.get(i), is(EXPECT.get(cnt)));
			//			cnt++;
			//			result.get(i);
			//			EXPECT.get(cnt);
			//カウント考えるc

		}

	}

	@Test
	public void ファイル読み込み時エラーalfCnt1TestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります：S,H,D,Eのいずれかの文字が入っていません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH1);
		String result = f.errmgs;

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーalfStestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります：Sが入っていません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH2);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーtimeフォーマットTestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります：時間がフォーマット通りではない、またはnullです";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH3);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーtime不正TestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります：時間がフォーマット通りではない、またはnullです";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH19);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーalfCnt2TestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります:2.1";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH4);
		String result = f.errmgs;

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーalfHtestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります:2,2";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH5);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーヘッダ個数合わないtestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります：ヘッダの数が要素数と合致していません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH6);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーalfCnt3TestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります3.1";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH7);
		String result = f.errmgs;

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーalfDtestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります:DでもEでもありません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH8);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void ファイル読み込み時エラーデータ個数合わないtestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります：ヘッダの数が要素数と合致していません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH9);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void レコード有効性チェック会議室名testReadCsvFile() throws Exception {
		String expect = "会議室名が正しく入力されていません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH10);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void レコード有効性チェック収容人数testReadCsvFile() throws Exception {
		String expect = "数字で入力されていません：capacity";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH11);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void レコード有効性チェックMic数testReadCsvFile() throws Exception {
		String expect = "数字で入力されていません：Mic";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH12);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void レコード有効性チェックWB数testReadCsvFile() throws Exception {
		String expect = "数字で入力されていません：board";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH13);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void レコード有効性チェックPj数testReadCsvFile() throws Exception {
		String expect = "数字で入力されていません：projector";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH14);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void レコード有効性チェック時間フォーマット問題testReadCsvFile() throws Exception {
		String expect = "正しい時間が入力されていません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH15);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void レコード有効性チェック時間時間不正問題testReadCsvFile() throws Exception {
		String expect = "正しい時間が入力されていません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH18);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void 数字かチェックtestReadCsvFile() throws Exception {
		String expect = "レコードの数字に問題があります";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH16);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void データ数チェックtestReadCsvFile() throws Exception {
		String expect = "CSVファイルに不正があります：データの数が一致しません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH17);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}

	@Test
	public void FNFExceptionを出す() throws Exception {
		FileReadMethod f = new FileReadMethod();
		f.readCsvFile(WPATH);
		String result = f.errmgs;
		String expect = "ファイルが見つかりませんでした";

		assertThat(result, is(expect));

	}

	@Test
	public void エンド行にデータが入っていないReadCsvFile() throws Exception {
		String expect = "データの数が入力されていません";
		FileReadMethod f = new FileReadMethod();

		f.readCsvFile(PATH20);
		String result = f.errmgs;
		System.out.println(result);

		assertThat(result, is(expect));

	}



}
